import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    a = 3 * np.ones(10, dtype=np.int32)
    b = 4 * np.ones(10, dtype=np.int32)
    c = np.zeros(1, dtype=np.int32)
else:
    a = None
    b = None
    c = None

el_a = np.zeros(1, dtype=np.int32)
el_b = np.zeros(1, dtype=np.int32)
comm.Scatter(a, el_a, root=0)
comm.Scatter(b, el_b, root=0)

el_c = el_a * el_b
comm.Reduce(el_c, c, op=MPI.SUM, root=0)

if rank == 0:
    print("Rezultat:", c)
    print("Provjera:", sum(a * b))
