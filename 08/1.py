import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    a = np.array([[1, 2], [3, 4], [5, 6]], dtype=np.int32)
    b = np.array([[9, 10, 11], [12, 13, 14]], dtype=np.int32)
    c = np.zeros((3, 3), dtype=np.int32)
else:
    a = np.zeros((3, 2), dtype=np.int32)
    b = np.zeros((2, 3), dtype=np.int32)
    c = None

comm.Bcast(a, root=0)
comm.Bcast(b, root=0)

el_c = np.dot(a[rank // 3], b[:, rank % 3])

comm.Gather(el_c, c, root=0)

if rank == 0:
    print("Rezultat:", c)
    print("Provjera:", np.dot(a, b))

