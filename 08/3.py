import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    a = 3 * np.ones(10, dtype=np.int32)
    b = 4 * np.ones(10, dtype=np.int32)
    c = np.zeros(1, dtype=np.int32)
    dijelovi_rjesenja = np.zeros(10, dtype=np.int32) #
else:
    a = None
    b = None
    c = None
    dijelovi_rjesenja = None #

el_a = np.zeros(1, dtype=np.int32)
el_b = np.zeros(1, dtype=np.int32)
comm.Scatter(a, el_a, root=0)
comm.Scatter(b, el_b, root=0)

el_c = el_a * el_b

"""
 * Zamijenite kod u nastavku ekvivalntnim kodom koji koristi
   comm.Gather; iskoristite dijelovi_rjesenja
"""

comm.Gather(el_c, dijelovi_rjesenja, root=0)

if rank == 0:
    print("Rezultat:", np.sum(dijelovi_rjesenja))
    print("Provjera:", np.sum(a * b))
