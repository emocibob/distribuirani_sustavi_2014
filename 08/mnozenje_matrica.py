import numpy as np

def mnozenjeMatrica(a, b, rezultat):
    for i in range(len(a)):
        for j in range(len(b[0])):
            rezultat[i][j] = np.dot(a[i], b[ : , j])


a = np.array([[1, 2], [3, 4], [5, 6]], dtype=np.int32)
b = np.array([[9, 10, 11], [12, 13, 14]], dtype=np.int32)
c = np.zeros((3, 3), dtype=np.int32)

mnozenjeMatrica(a, b, c)

print("Rezultat:", c)
print("Provjera:", np.dot(a, b))

a = a.reshape((2, 3))
b = b.reshape((3, 2))
c = np.zeros((2, 2), dtype=np.int32)

mnozenjeMatrica(a, b, c)

print("Rezultat:", c)
print("Provjera:", np.dot(a, b))
