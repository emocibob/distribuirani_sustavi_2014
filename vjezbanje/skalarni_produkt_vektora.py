"""
Dodajte kod za još dvije redukcije:

prva neka nalazi najmanju vrijednost među dobivenim produktima,

druga neka nalazi najveću vrijednost među dobivenim produktima.

(Uputa: op=MPI.MIN i op=MPI.MAX)
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    a = np.array([1, 2, 3, 9, 10, 11], dtype=np.float32)
    b = np.array([4, 5, 6, 7, 8, 24], dtype=np.float32)
    s_produkt = np.zeros(1, dtype=np.float32)
    v_min = np.zeros(1, dtype=np.float32)
    v_max = np.zeros(1, dtype=np.float32)
else:
    a = None
    b = None
    s_produkt = None
    v_min = None
    v_max= None

a_part = np.zeros(1, dtype=np.float32)
b_part = np.zeros(1, dtype=np.float32)
comm.Scatter(a, a_part, root=0)
comm.Scatter(b, b_part, root=0)

s_part = a_part * b_part
comm.Reduce(s_part, s_produkt, op=MPI.SUM, root=0)
comm.Reduce(s_part, v_min, op=MPI.MIN, root=0)
comm.Reduce(s_part, v_max, op=MPI.MAX, root=0)

if rank == 0:
    print(a)
    print(b)
    print(s_produkt)
    print("min:", v_min, ", max:", v_max)
