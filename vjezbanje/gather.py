"""
Promijenite program da se izvodi u 5 procesa, i to tako da svaki od procesa inicijalizira listu, odnosno polje, duljine 4 u kojem je prvi element njegov rang, a ostali elementi su slučajne vrijednosti u rasponu od 0.0 do 1.0.

Napravite operaciju gather sa korijenskim procesom ranga 3, te operaciju allgather. Svi procesi neka oba rezultata ispišu na ekran.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

sendmsg = np.array([rank, np.random.rand(), np.random.rand(), np.random.rand()], dtype=np.float32)

print("Proces ranga", rank, "ima vrijednost poruke za slanje", sendmsg)

if rank == 3:
    recvmsg1 = np.empty((5, 4), dtype=np.float32)
else:
    recvmsg1 = None

comm.Gather(sendmsg, recvmsg1, root=3)
    
recvmsg2 = np.empty((5, 4), dtype=np.float32)

comm.Allgather(sendmsg, recvmsg2)

print("Proces ranga", rank, "ima vrijednost prve primljene poruke", recvmsg1, "i druge primljene poruke", recvmsg2)
