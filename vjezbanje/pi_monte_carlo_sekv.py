import numpy as np

n = 10000
h = 0
throws = 0

for i in range(n):
    x = np.random.rand()
    y = np.random.rand()
    if np.sqrt(x**2 + y**2) <= 1:
        h += 1
    throws += 1

pi = 4 * (h / throws)
print(pi)
