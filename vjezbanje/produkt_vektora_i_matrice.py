import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    A = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [11, 22, 33]], dtype=np.float32)
    x = np.array([10, 11, 12], dtype=np.float32)
    y = np.empty(4, dtype=np.float32)
else:
    A = None
    x = np.zeros(3, dtype=np.float32)
    y = None

a_part = np.zeros(3, dtype=np.float32)
comm.Scatter(A, a_part, root=0)
comm.Bcast(x, root=0)

y_part = np.dot(a_part, x)
comm.Gather(y_part, y, root=0)

if rank == 0:
    print(y)
    print(y == np.dot(A, x))
