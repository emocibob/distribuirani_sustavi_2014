"""
Napišite program koji koristi MPI za izračun zbroja kvadrata brojeva u rasponu od 1 do 500000 u 4 procesa korištenjem kolektivne komunikacije tipa scatter-reduce. Raspodijelite po želji; za to morate napraviti listu oblika

brojevi = [[1, 2, 3, ..., 125000],
           [125001, 125002, ..., 250000],
           [250001, 250002, ..., 375000],
           [375001, 375002, ..., 500000]]

odnosno potrebno je da ima 4 podliste od kojih će savka biti dana odgovarajućem procesu. Svi procesi računaju zbroj kvadrata brojeva koje su dobili. Nakon završetka obrade na procesu ranga 0 sakupite rezultate i to tako da izvršite redukciju korištenjem sumiranja.
"""
from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size != 4:
    print("Potrebno 4 procesa.")
    exit()

if rank == 0:
    brojevi = np.array([[x for x in range(1, 125001)],
                        [x for x in range(125001, 250001)],
                        [x for x in range(250001, 375001)],
                        [x for x in range(375001, 500001)]], dtype=np.int64)
    suma = np.zeros(1, dtype=np.int64)
else:
    brojevi = None
    suma = None

b_part = np.zeros(125000, dtype=np.int64)
comm.Scatter(brojevi, b_part, root=0)

suma_part = 0
for i in b_part:
    suma_part += i ** 2

comm.Reduce(suma_part, suma, op=MPI.SUM, root=0)

if rank == 0:
    print(suma)
    s_p = 0
    for i in range(500001):
        s_p += i**2
    print(suma == s_p)
