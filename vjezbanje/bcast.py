"""
Prilagodite kod primjera tako da se broadcastom šalje lista s 4 podliste s po 4 elementa, odnosno polje oblika (4, 4), u oba slučaja s proizvoljnim vrijednostima.

Učinite da nakon broadcasta proces ranga 2 promijeni vrijednost elementa na poziciji (1, 1) tako da je uveća za 5, i zatim napravite broadcast s njega svim ostalima.

Inicijalizirajte na procesu ranga 1 polje formata (5, 15) kodom np.array([list(range(x, x + 15)) for x in range(5)]) i napravite broadcast s njega svim ostalima (pripazite na tip polja u koje primate). Učinite da svaki od procesa po primitku polja ispisuje na ekran vrijednost na koordinati (rang, rang).

Na svim procesima ispišite primljene podatke na ekran da provjerite je li operacija bila uspješna.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    a = np.arange(1, 17).reshape((4, 4))
    a = a.astype(np.int32)
    b = np.zeros((5, 15), dtype=np.int32)
elif rank == 1:
    a = np.zeros((4, 4), dtype=np.int32)
    b = np.array([list(range(x, x + 15)) for x in range(5)], dtype=np.int32)
    print(b)
else:
    a = np.zeros((4, 4), dtype=np.int32)
    b = np.zeros((5, 15), dtype=np.int32)
    
#print(rank, "- prije:", a)
comm.Bcast(a, root=0)
#print(rank, "- nakon:", a)

if rank == 2:
    a[1][1] += 5

comm.Bcast(a, root=2)
print(a, "\n")

comm.Bcast(b, root=1)
print(rank, b[rank][rank])
