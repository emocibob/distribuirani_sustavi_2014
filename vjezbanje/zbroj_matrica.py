"""
Za matrice a i b moguće je odrediti njihov zbroj na način:

# varijanta s NumPy poljima
a = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]], dtype=np.int32)
b = np.array([[10, 20, 30], [40, 50, 60], [70, 80, 90]], dtype=int32)
zbroj = np.zeros((3, 3), dtype=np.int32)

for i in range(3):
    for j in range(3):
        zbroj[i][j] = a[i][j] + b[i][j]

# zbroj = a + b

Primijenite kolektivnu komunikaciju tipa scatter-gather da zbroj matrica računate u 3 procesa.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    a = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]], dtype=np.int32)
    b = np.array([[10, 20, 30], [40, 50, 60], [70, 80, 90]], dtype=np.int32)
    zbroj = np.zeros((3, 3), dtype=np.int32)
else:
    a = None
    b = None
    zbroj = None

a_part = np.zeros(3, dtype=np.int32)
b_part = np.zeros(3, dtype=np.int32)
comm.Scatter(a, a_part, root=0)
comm.Scatter(b, b_part, root=0)

z_part = a_part + b_part
comm.Gather(z_part, zbroj, root=0)

if rank == 0:
    print(zbroj)
    print(zbroj == a + b)
