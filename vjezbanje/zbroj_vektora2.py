"""
Učinite da program umjesto zbroja vektora računa produkt vektora po elementima, odnosno vektor u kojem je svaki element produkt elemenata u vektorima s pripadnim indeksom.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size != 6:
    print("Potrebno 6 procesa.")
    exit()

if rank == 0:
    a = np.array([1, 2, 3, 9, 10, 20], dtype=np.int32)
    b = np.array([4, 5, 6, 7, 30, 40], dtype=np.int32)
    c = np.array([100, 200, 300, 400, 500, 600], dtype=np.int32)
    zbroj_vektor = np.empty(6, dtype=np.int32)
else:
    a = None
    b = None
    c = None
    zbroj_vektor = None

a_part = np.zeros(1, dtype=np.int32)
b_part = np.zeros(1, dtype=np.int32)
c_part = np.zeros(1, dtype=np.int32)
comm.Scatter(a, a_part, root=0)
comm.Scatter(b, b_part, root=0)
comm.Scatter(c, c_part, root=0)

z_part = a_part * b_part * c_part
comm.Gather(z_part, zbroj_vektor, root=0)

if rank == 0:
    print(a)
    print(b)
    print(c)
    print(zbroj_vektor)
