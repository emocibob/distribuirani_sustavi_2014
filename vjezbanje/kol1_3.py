"""
3. Implementirajte zbroj dvaju matrica korištenjem MPI kolektivne komunikacije tipa Scatter-Gather.
"""
from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size != 4:
   print("=/= 4 procesa")
   exit()

if rank == 0:
    a = np.arange(20).reshape((4, 5)).astype(np.int32)
    b = np.arange(20, 40).reshape((4, 5)).astype(np.int32)
    c = np.zeros((4, 5), dtype=np.int32)
else:
    a = None
    b = None
    c = None

a_part = np.zeros(5, dtype=np.int32)
b_part = np.zeros(5, dtype=np.int32)
comm.Scatter(a, a_part, root=0)
comm.Scatter(b, b_part, root=0)

c_part = a_part + b_part
comm.Gather(c_part, c, root=0)

if rank == 0:
   print(a)
   print(b)
   print(c)
   print(c == a + b)
