"""
Modificirajte gornji primjer tako da dodate kod koji mjeri vrijeme izvođenja za svaki od procesa.

Usporedite vrijeme izvođenja algoritma za 2, 3, 4 procesa kad svaki proces izvodi 104, 105, 106 iteracija. Opišite svoje zaključke.
"""
import numpy as np
from mpi4py import MPI
import time

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

n_iterations = 10000

def compute_pi(n, start=0, step=1):
    h = 1.0 / n
    s = 0.0
    for i in range(start, n, step):
        x = h * (i + 0.5)
        s += 4.0 / (1.0 + x**2)
    return s * h

pi_part = np.zeros(1, dtype=np.float64)
start = time.time()
pi_part[0] = compute_pi(n_iterations, start=rank, step=size)
end = time.time()
print(end - start, "s")

if rank == 0:
    pi = np.zeros(1, dtype=np.float64)
else:
    pi = None

comm.Reduce(pi_part, pi, op=MPI.SUM, root=0)

if rank == 0:
    error = abs(pi - np.math.pi)
    print("pi:", pi, ", error:", error)
