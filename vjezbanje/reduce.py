"""
Promijenite kod tako da:

na glavnom procesu se inicijalizira lista ili polje slučajnih vrijednosti, koje se raspršuju (scatter) na sve procese,

svaki od procesa kvadrira vrijednost koju primi,

zatim se vrši redukcija na proces ranga 0 operacijom MIN i redukcija na sve procese operacijom MAX.

Neka se takav program izvodi u 8 procesa. Alternativno, napravite da se može izvoditi u proizvoljnom broju procesa.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    sendmsg = np.array(list(np.random.rand() for i in range(size)), dtype=np.float32)
    #print(sendmsg)
    red1 = np.zeros(1, dtype=np.float32)
else:
    sendmsg = None
    red1 = None

msg1 = np.zeros(1, dtype=np.float32)

comm.Scatter(sendmsg, msg1, root=0)
#print(rank, sendmsg, msg1)

msg1 = msg1**2
print(rank, msg1)

comm.Reduce(msg1, red1, op=MPI.MIN, root=0)

red2 = np.zeros(1, dtype=np.float32)
comm.Allreduce(msg1, red2, op=MPI.MAX)

if rank == 0:
    print("Min:", red1)
    print("Max:", red2)
