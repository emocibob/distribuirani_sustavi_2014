"""
Napišite program koji koristi MPI za izračun zbroja kubova brojeva u rasponu od 1 do 300000 u 6 procesa korištenjem kolektivne komunikacije tipa scatter-reduce. Na procesu ranga 0 inicijalizirajte listu pojedinih raspona i raspodijelite je procesima koji kubiraju dobivene brojeve i zbrajaju ih. Nakon završetka obrade na procesu ranga 0 izvedite redukciju sumiranjem i na procesu ranga 0 ispišite rezultat na ekran. Ostali procesi neka ne ispisuju ništa.
"""
from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size != 6:
    print("Potrebno 6 procesa.")
    exit()

if rank == 0:
    a = np.array([[x for x in range(1, 50001)],
                  [x for x in range(50001, 100001)],
                  [x for x in range(100001, 150001)],
                  [x for x in range(150001, 200001)],
                  [x for x in range(200001, 250001)],
                  [x for x in range(250001, 300001)]], dtype=np.int64)
    suma = np.zeros(1, dtype=np.int64)
else:
    a = None
    suma = None

a_part = np.zeros(50000, dtype=np.int64)
comm.Scatter(a, a_part, root=0)

s_part = 0
for i in a_part:
    s_part += i**2 # **3

comm.Reduce(s_part, suma, op=MPI.SUM, root=0)

if rank == 0:
    print(suma)
    s_p = 0
    for i in range(1, 300001):
        s_p += i**2 # **3
    print(suma == s_p)
