import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

n_random_choices = 100000
hits = 0
throws = 0

for i in range (0, n_random_choices):
    throws += 1
    x = np.random.random()
    y = np.random.random()
    dist = np.math.sqrt(x * x + y * y)
    if dist <= 1.0:
        hits += 1

pi_part = np.empty(1)
pi_part[0] = 4 * (hits / throws)

if rank == 0:
    pi_reduced = np.empty(1)
else:
    pi_reduced = None

comm.Reduce(pi_part, pi_reduced, op=MPI.SUM, root=0)

if rank == 0:
    pi = pi_reduced[0] / size
    error = abs(pi - np.math.pi)
    print("pi is approximately %.16f, error is approximately %.16f" % (pi, error))
