"""
1. Napravite datoteku podaci.txt iz koje ćete korištenjem numpy-a učitati podatke. Podaci neka
   budu cjelobrojni i neka čine matricu sa 6 redaka i 3 stupca. Učitajte tu matricu dvaput, i to
   tako da prvo polje koje dobijete ima vrijednosti tipa int32, a drugo vrijednosti tipa float64.
   Program neka ispisuje:
 * posljednji redak prvog polja pretvoren u tip int64,
 * prvi stupac drugog polja pretvoren u tip float32,
 * prvo polje pretvoreno u oblik sa 3 retka i 6 stupaca.
 * sravnjeno drugo polje,
 * rezultat zbroja tih dvaju polja element po element, i njegov tip,
 * sumu elemenata prvog polja,
 * rezultat funkcije kosinus element po element, na elementima drugog polja,
 * dot-produkt (produkt matrica) prvog polja i transponiranog drugo polja, i njegov tip.
"""
import numpy as np

p1 = open("podaci.txt")
p2 = open("podaci.txt")
a = np.loadtxt(p1).astype(np.int32)
b = np.loadtxt(p2).astype(np.float64)
p1.close()
p2.close()

print(a, a.dtype)
print(b, b.dtype, "\n")

print(a[-1].astype(np.int64))

print(b[ : , 0].astype(np.float32))

print(np.reshape(a, (3, 6)))

print(np.ravel(b))

c = a + b
print(c, c.dtype)

suma = sum(sum(a))
print(suma)

d = np.cos(b)
print(d)

e = np.dot(a, np.transpose(b))
print(e, e.dtype)
