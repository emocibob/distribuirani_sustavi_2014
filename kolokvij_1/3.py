"""
3. Implementirajte zbroj dvaju matrica korištenjem MPI kolektivne komunikacije tipa Scatter-Gather.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size != 4:
    print("Potrebno 4 procesa.")
    exit()

if rank == 0:
    a = np.array([np.arange(1, 5),
                   np.arange(10, 14),
                   np.arange(21, 25),
                   np.arange(100, 500, 100)], dtype=np.int32)
    b = np.array([np.arange(10, 50, 10),
                   np.arange(30, 34),
                   np.arange(100, 104),
                   np.arange(1000, 5000, 1000)], dtype=np.int32)
    zbroj = np.zeros((4, 4), dtype=np.int32)
else:
    a = None
    b = None
    zbroj = None

a_dio = np.zeros(4, dtype=np.int32)
b_dio = np.zeros(4, dtype=np.int32)
comm.Scatter(a, a_dio, root=0)
comm.Scatter(b, b_dio, root=0)

z_dio = a_dio + b_dio
comm.Gather(z_dio, zbroj, root=0)
if rank == 0:
    print("Zbroj:", zbroj)
    #print("Provjera:", zbroj == a + b)
