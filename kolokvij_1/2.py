"""
2. Napravite MPI program koji koristi blokirajuću komunikaciju tipa točka-do-točke za
   izračun zbroja i produkta element po element dvaju vektora veličine 12 elemenata.
   Vektori su reprezentirani numpy poljima. Vektori se inicijaliziraju na procesu ranga
   0, a zatim on:

 * procesu ranga 1 šalje prvih 6 elemenata oba vektora i on ih zbraja element po element
 i vraća rezultat procesu ranga 0,

 * procesu ranga 2 šalje posljednjih 6 elemenata oba vektora i on ih zbraja element po
 element i vraća rezultat procesu ranga 0,

 * procesu ranga 3 šalje prvih 6 elemenata oba vektora i on ih množi element po element
 i vraća rezultat procesu ranga 0,

 * procesu ranga 4 šalje posljednjih 6 elemenata oba vektora i on ih množi element po
 element i vraća rezultat procesu ranga 0.

Proces ranga 0 ispisuje zbroj i produkt na ekran.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 5:
    print("Potrebno barem 5 procesa.")
    exit()

if rank == 0:
    a = np.array(np.arange(1, 13), dtype=np.int32)
    b = np.array(np.arange(12, 24), dtype=np.int32)
    zbroj = np.zeros(12, dtype=np.int32)
    produkt = np.zeros(12, dtype=np.int32)

    comm.Send(a[0:6], dest=1, tag=1)
    comm.Send(b[0:6], dest=1, tag=2)
    comm.Send(a[6:12], dest=2, tag=1)
    comm.Send(b[6:12], dest=2, tag=2)

    t = np.zeros(6, dtype=np.int32)
    comm.Recv(t, source=1)
    zbroj[0:6] = t
    comm.Recv(t, source=2)
    zbroj[6:12] = t

    comm.Send(a[0:6], dest=3, tag=1)
    comm.Send(b[0:6], dest=3, tag=2)
    comm.Send(a[6:12], dest=4, tag=1)
    comm.Send(b[6:12], dest=4, tag=2)

    comm.Recv(t, source=3)
    produkt[0:6] = t
    comm.Recv(t, source=4)
    produkt[6:12] = t    

    #print(a)
    #print(b)
    print("Zbroj:", zbroj)
    print("Produkt:", produkt)
    #print("Provjere:", zbroj == a + b, produkt == a * b)

elif rank == 1 or rank == 2:
    a_dio = np.zeros(6, dtype=np.int32)
    b_dio = np.zeros(6, dtype=np.int32)

    comm.Recv(a_dio, source=0, tag=1)
    comm.Recv(b_dio, source=0, tag=2)

    z_dio = a_dio + b_dio
    comm.Send(z_dio, dest=0)

elif rank == 3 or rank == 4:
    a_dio = np.zeros(6, dtype=np.int32)
    b_dio = np.zeros(6, dtype=np.int32)

    comm.Recv(a_dio, source=0, tag=1)
    comm.Recv(b_dio, source=0, tag=2)

    p_dio = a_dio * b_dio
    comm.Send(p_dio, dest=0)

else:
    exit()
