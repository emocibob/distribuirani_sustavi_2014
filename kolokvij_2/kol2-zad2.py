"""
2. Implementirajte Monte Carlo simulaciju koja računa površinu kvadrata stranice duljine 0.5
 i paralelizirajte je korištenjem operacije Reduce.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

n_iterations = 100000

def compute_area(n, start=0, step=1):
    hits = 0
    for i in range(start, n, step):
        x = np.random.rand()
        y = np.random.rand()
        if x < 0.5 and y < 0.5:
            hits += 1
    return hits / n

if rank == 0:
    result = np.empty(1)
else:
    result = None

part = np.zeros(1, dtype=np.float64)
part[0] = compute_area(n_iterations, start=0, step=1)
comm.Reduce(part, result, op=MPI.SUM, root=0)

if rank == 0:
    result[0] = result[0] / size
    error = abs(result - 0.25)
    print("Result is approximately %.16f, error is approximately %.16f" % (result, error))
