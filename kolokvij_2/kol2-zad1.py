"""
1. Implementirajte produkt dvaju matrica korištenjem MPI kolektivne komunikacije.
 Tip kolektivne komunikacije odaberite po vlastitom nahođenju (Broadcast, Scatter, Gather,
 Reduce).
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size != 3:
    print("Potrebno 3 procesa.")
    exit()

if rank == 0:
    A = np.array([[1, 2],
                  [4, 5],
                  [10, 15]], dtype=np.int32)
    B = np.array([[12, 14, 17],
                  [25, 56, 42]], dtype=np.int32)
    C = np.zeros((3, 3), dtype=np.int32)
else:
    A = None
    B = np.zeros((2, 3), dtype=np.int32)
    C = None

A_red = np.zeros(2, dtype=np.int32)
comm.Scatter(A, A_red, root=0)
comm.Bcast(B, root=0)

C_part = np.dot(A_red, B)
comm.Gather(C_part, C, root=0)

if rank == 0:
    print("Produkt matrice\n", A)
    print("i matrice\n", B)
    print("iznosi\n", C)
    #print("Provjera:", C == np.dot(A, B))
