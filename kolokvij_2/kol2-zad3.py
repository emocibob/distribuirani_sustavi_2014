"""
3. Odaberite program iz prvog zadatka ili program iz drugog zadatka i u okviru ovog zadatka
 dodajte kod koji mjeri vrijeme izvođenja. Mjerite vrijeme izvođenja za fiksan broj procesa
 za četiri različite veličine problema (veličine matrica ili broj iteracija simulacije).
 Spremite rezultate u tekstualnu datoteku i ukratko opišite zapažanja.

 Veličine neka se međusobno razlikuju za faktor 10, a odaberite ih tako da izvođenje najvećeg
 slučaja traje duže od 10, a kraće od 30 sekundi. Osvrt na mjerenja zapišite u komentaru
 unutar .py datoteke.
"""
import numpy as np
from mpi4py import MPI
import time

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

n_iterations = 100000

def compute_area(n, start=0, step=1):
    hits = 0
    for i in range(start, n, step):
        x = np.random.rand()
        y = np.random.rand()
        if x < 0.5 and y < 0.5:
            hits += 1
    return hits / n

if rank == 0:
    result = np.empty(1)
else:
    result = None

part = np.zeros(1, dtype=np.float64)
start = time.time()
part[0] = compute_area(n_iterations, start=0, step=1)
end = time.time()
comm.Reduce(part, result, op=MPI.SUM, root=0)

if rank == 0:
    result[0] = result[0] / size
    error = abs(result - 0.25)
    print("Result is approximately %.16f, error is approximately %.16f" % (result, error))
    print("Br. procesa:", size, ", n =", n_iterations)
    print("Time:", end - start, "s")

"""
 * Izlazi:

 [NB: izlaze sam u kodu za repo maknuo,
      na kolokviju sam program pokrenuo 4 puta - svaki put sa 4 procesa,
      n-ovi su redom bili: 1000000, 100000, 10000, 1000]

 * Osvrt:

Za 4 procesa smanjenje broja iteracija simulacije smanjuje vrijeme izvođenja programa (pošto
 za svaki proces vrijeme izvođenja ovisi samo o broju iteracija to ima smisla).
Preciznost rezultata je bila najveća sa 100000 iteracija, što govori da do neke granice ima
 smisla povećavati broj iteracija sa ciljem povećanja preciznosti. Nakon te granice greške
 zaokruživanja float aritmetike rezultiraju smanjenjem preciznosti rješenja.
"""
