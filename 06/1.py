"""
 * Modificirajte kod tako da se izvodi u 5 procesa, a scatter se
   vrši nad matricom proizvoljnih vrijednosti koja ima 5 redaka
   i 4 stupca zapisanom u obliku liste, odnosno numpy polja.

 * Učinite da svaki od procesa posljednji element u primljenom
   retku matrice postavlja na vrijednost 0, a zatim ispisuje taj
   redak na ekran.
"""
# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size != 5:
    print("Potrebno 5 procesa.")
    exit()

if rank == 0:
    sendmsg = np.array([[1, 2, 3, 4],
                        [5, 6, 7, 8],
                        [9, 10, 11, 12],
                        [10, 20, 30, 40],
                        [50, 60, 70, 80]], dtype=np.int32)
else:
    sendmsg = None

recvmsg = np.zeros(4, dtype=np.int32)
comm.Scatter(sendmsg, recvmsg, root=0)
recvmsg[-1] = 0
print("Proces ranga", rank, "ima vrijednost poruke za slanje", sendmsg, "primljena poruka", recvmsg, "\n")
