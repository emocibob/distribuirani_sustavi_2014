# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

sendmsg = np.array([rank**2], dtype=np.int32)

print("Proces ranga", rank, "ima vrijednost poruke za slanje", sendmsg)

if rank == 0:
    recvmsg1 = np.empty(size, dtype=np.int32)
else:
    recvmsg1 = None

comm.Gather(sendmsg, recvmsg1, root=0)

recvmsg2 = np.empty(size, dtype=np.int32)

comm.Allgather(sendmsg, recvmsg2)

print("Proces ranga", rank, "ima vrijednost prve primljene poruke", recvmsg1, "i druge primljene poruke", recvmsg2)
