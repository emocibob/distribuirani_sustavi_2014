# Git

## Najčešće korištene naredbe

- `git pull`
- `git status`
- `git add <file>`
- `git commit -m "<message>"`
- `git push origin master`

## Prvo postavljanje

- postavljanje identiteta:
    
        git config --global user.name "<name>"
        git config --global user.email <email>
    
	- `<name>` - ime i prezime
    - `<email>` - email adresa
    - ako se želi postaviti identitet za samo jedan repo umjesto `global` koristi se `local`
    
- `git clone <repo>` - kloniranje repoa na računalo
    - `<repo>` - link repozitorija

## Osnovne naredbe

- `git pull` - preuzmi zadnju verziju centralnog repoa u svoj lokalni repo

- `git status` - prikaz statusa repoa (vidi izmjene)

- `git diff` - pogledaj izmjene između commitova
    - `git diff --word-diff` - pogledaj izmjene na razini riječi

- `git add <file>` - dodaj datoteku (ili direktorij) za commit

- `git commit` - snimanje izmjena u repozitoriju
    - naredbom se također otvara text editor za ubacivanje poruke commita (konvencija: naslov, prazan red, paragraf opisa)
    - `git commit -m "<message>"` - komitaj odmah sa porukom `<message>`
    - `git commit --amend` - dodaj u zadnji commit još neke izmjene ili samo izmjeni poruku za commit

- `git log` - pregled povijesti commitova

- `git push origin master` - gurni promjene na centralni repo
    - `origin` - remote repozitorij (Bitbucket server)
    - `master` - (lokalna) grana za pushanje

## Grane

- `git branch <branch>` - napravi granu pod imenom `<branch>`

- `git checkout <existing-branch>` - premjesti se na (postojeću) granu `<existing-branch>`

- `git branch` - pogledaj sve grane te na kojoj se nalazim

- `git merge <branch>` - spoji `<branch>` na trenutnu granu

## Ostalo

- `git init` - stvori prazan git repozitorij ili reinicijaliziraj postojeći
- `git remote show origin` - prikaži informacije za remote repozitorij
    - `origin` - alias za remote repozitorij

## Ignoriranje datoteka u lokalnom repozitoriju

- da bi se lokalne datoteke ignorirale kod pokretanja `git status` treba imena tih datoteka staviti u tekstualnu datoteku nazvanu `.gitignore`
    - svaki redak u `.gitignore` je jedan uzorak za ignoriranje
    - kad bi npr. htjeli da se ignoriraju sve PDF i JPG datoteke u našem repou, `.gitignore` bi izgledao ovako:
        
            *.pdf
            *.jpg
        
## Primjeri

### Već imam Git repo koji treba staviti na Bitbucket

```
cd /path/to/my/repo
git remote add origin ssh://git@bitbucket.org/username/bbreponame.git
git push -u origin --all
```

### Trebam u mapi postaviti novi Git repo i staviti ga na Bitbucket

```
cd /path/to/your/project
git init
git remote add origin ssh://git@bitbucket.org/username/bbreponame.git
```