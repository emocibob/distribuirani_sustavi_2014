import numpy as np
from mpi4py import MPI
from sys import argv

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

try:
    n_random_choices = int(argv[1])
except IndexError:
    n_random_choices = 10000
hits = 0
throws = 0

print("Each process will do", n_random_choices // size, "throws, total", size * (n_random_choices // size), "throws.")

for i in range (0, n_random_choices // size):
    throws += 1
    x = np.random.random()
    y = np.random.random()
    dist = np.math.sqrt(x * x + y * y)
    if dist <= 1.0:
        hits += 1

pi_part = np.empty(1)
pi_part[0] = 4 * (hits / throws)

if rank == 0:
    pi_reduced = np.empty(1)
else:
    pi_reduced = None

comm.Reduce(pi_part, pi_reduced, op=MPI.SUM, root=0)

if rank == 0:
    pi = pi_reduced[0] / size
    error = abs(pi - np.math.pi)
    print("pi is approximately %.16f, error is approximately %.16f" % (pi, error))
