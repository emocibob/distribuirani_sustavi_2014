import numpy as np
from sys import argv

try:
    n_random_choices = int(argv[1])
except IndexError:
    n_random_choices = 10000
hits = 0
throws = 0

for i in range (0, n_random_choices):
    throws += 1
    x = np.random.random() # x = random.random()
    y = np.random.random() # y = random.random()
    dist = np.math.sqrt(x * x + y * y) # dist = math.sqrt(x * x + y * y)
    if dist <= 1.0:
        hits += 1

pi = 4 * (hits / throws)

error = abs(pi - np.math.pi) # error = abs(pi - math.pi)
print("pi is approximately %.16f, error is approximately %.16f" % (pi, error))
 
