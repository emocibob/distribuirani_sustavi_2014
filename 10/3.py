"""
 * Stvorite grupu procesa koja sadrži samo procese iz grupe COMM_WORLD koji imaju parni rang.
   Iskoristite novu grupu da bi stvorili novi komunkator. (Uputa: iskoristite Group.Incl(),
   Group.Range_incl() ili Group.Excl().)
"""
from mpi4py import MPI

comm = MPI.COMM_WORLD
group = comm.Get_group()
size = comm.Get_size()
rank = comm.Get_rank()

newgroup = group.Excl(list(range(1, size, 2)))
newcomm = comm.Create(newgroup)

""" TODO popravit
group2 = group.Free()

newgroup2 = group2.Incl(list(range(0, size, 2)))
newcomm2 = comm2.Create(newgroup2)

if rank == 0:
    assert newcomm.Get_size() == newcomm2.Get_size()
"""
