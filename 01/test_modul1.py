import modul1
#from modul1 import zbrajanje

assert modul1.zbrajanje(2, 3) == 5
assert modul1.zbrajanje(23, 4) == 27
assert modul1.zbrajanje(24, 81) == 105

print("Testovi funkcije zbrajanje uspješno izvršeni.")

"""
 * Napišite po dva testa za funkcije množenje i ptenciranje.
   Iskoristite Calculator da bi lakše izračunali vrijednosti.
"""

assert modul1.mnozenje(11, 0) == 0
assert modul1.mnozenje(5, 11) == 55

print("Testovi funkcije mnozenje uspješno izvršeni.")

assert modul1.potenciranje(2, 10) == 1024
assert modul1.potenciranje(5, 3) == 125 

print("Testovi funkcije potenciranje uspješno izvršeni.")

assert round(modul1.dijeljenje(4, 5), 1) == 0.8
assert modul1.dijeljenje(20, 5) == 4
assert round(modul1.dijeljenje(2, 3), 2) == 0.67

"""
 * Dodajte još jedan test sa dva broja koji nisu cjelobrojni.
   Zaokružite rješenje na 5 decimala.
"""

assert round(modul1.dijeljenje(10.5, 3.1), 5) == 3.38710

print("Testovi funkcije dijeljenje uspješno izvršeni.")
