def zbrajanje(x, y):
    """
    Funkcija vrši zbrajanje dva broja.

    Argumenti:
    x -- prvi broj
    y -- drugi broj
    
    Vraća:
    Zbroj dva broja
    """
    return x + y

"""
 * Dokumentirajte preostale funkcije
"""

def mnozenje(x, y):
    """
    Funkcija vrši množenje dva broja.

    Argumenti:
    x -- prvi broj
    y -- drugi broj
    
    Vraća:
    Umnožak dva broja
    """
    return x * y

def potenciranje(x, y):
    """
    Funkcija vrši potenciranje dva broja.

    Argumenti:
    x -- baza
    y -- eksponent
    
    Vraća:
    Potencija dva broja
    """    
    return x ** y

def dijeljenje(x, y):
    """
    Funkcija vrši dijeljenje dva broja.

    Argumenti:
    x -- djeljenjik
    y -- djelitelj
    
    Vraća:
    Količnik dva broja
    """
    return x / y
