- autor: Edvin Močibob
- kolegij: Distribuirani sustavi
- ak. god.: 2014./2015.
- jezik: Python3
- materijali: [reStructuredHgWiki](http://inf2.uniri.hr/reStructuredHgWiki/kolegiji/DS.html#vjezbe)
- pomoćna dokumentacija: [akari](http://inf2.uniri.hr:8080/)

-------------------------

Repozitorij za vježbe iz kolegija Distribuirani sustavi.

Za sažetak wikija pogledati `TLDR.md`.
