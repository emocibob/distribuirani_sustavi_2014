# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI
from sys import argv
from time import time

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

try:
    n_iterations = int(argv[1])
except IndexError:
    n_iterations = 1000

def compute_pi(n, start=0, step=1):
    h = 1.0 / n
    s = 0.0
    for i in range(start, n, step):
        x = h * (i + 0.5)
        s += 4.0 / (1.0 + x**2)
    return s * h

start = time()
pi_part = np.empty(1)
pi_part[0] = compute_pi(n_iterations, start=rank, step=size)

if rank == 0:
    pi = np.empty(1)
else:
    pi = None

comm.Reduce(pi_part, pi, op=MPI.SUM, root=0)
end = time()

if rank == 0:
    error = abs(pi - np.math.pi)
    print("pi is approximately %.16f, error is approximately %.16f" % (pi, error))
    print("Vrijeme izvođenja:", end - start)

