## Distribuirano izvođenje (i SSH bez passworda)

- ssh na akston: `ssh studentXY@akston`

- provjera hostova: `$ cat /etc/openmpi/openmpi-default-hostfile`

- generiraj ssh key: `$ ssh-keygen`

- zatim za svaki host (zadnji argument): `$ ssh-copy-id -i ~/.ssh/id_rsa.pub akston.local`

- provjera: `$ mpirun -np 5 hostname`

- distribuirani datotečni susatv na akstonu: `$ cd /lab336-gv0/student04/`

- test: `$ mpirun -np 5 pwd`

- primjer izvođenja: `$ mpirun -np 5 python3 pi1.py`

- lokalno izvođenje: `$ mpirun -hostfile hflocal -np 4 python3 pi1.py 100000`

  - treba prije napraviti file `hflocal` koji sadrži u sebi `localhost`