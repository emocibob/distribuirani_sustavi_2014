# Wiki TL;DR

## NumPy

- `import numpy as np`

- `a = np.array([10, 20, 30, 40], dtype=np.int32)`
    - `a.shape` - oblik
    - `a.ndim` - broj dimenzija
    - `a.size` - broj elemenata
    - `a.dtype` - tip podataka
    - `a.itemsize` - veličina (u bajtovima) tipa podataka elemenata

- `np.zeros((n, m), dtype=...)`

- `np.ones((n, m), dtype=...)`

- `b = a.astype(np.float64)`

- `np.round(polje, broj_decimala)` - zaokruživanje

- `np.dot(a, b)` - matrično množenje

- spremi matricu iz datoteke u varijablu:

```
p = open("podaci.txt")
matrica = np.loadtxt(p)
p.close()
```

- `arange`:

```
>>> np.arange(3)
array([0, 1, 2])
>>> np.arange(3, 7)
array([3, 4, 5, 6])
>>> np.arange(3, 8, 2)
array([3, 5, 7])
```

- `ravel`, `transpose`, `reshape`, `resize`:

```
>>> a
array([[1, 2, 3],
       [4, 5, 6]])
>>> np.ravel(a) # sravnavanje polja
array([1, 2, 3, 4, 5, 6])
>>> np.transpose(a) # transponiranje
array([[1, 4],
       [2, 5],
       [3, 6]])
>>> np.reshape(a, (3, 2)) # promjena oblika, ne mijenja podatke u polju
array([[1, 2],
       [3, 4],
       [5, 6]])
>>> np.resize(a, (3, 2)) # promjena oblika, ako dimenzije prevelike sam popunjava prazna mjesta
array([[1, 2],
       [3, 4],
       [5, 6]])							       
```

- `vsplit`, `hsplit`, `vstack`, `hstack`:

```
>>> a
array([[1, 2, 3, 4],
       [5, 6, 7, 8]])
>>> b = np.vsplit(a, 2)
>>> b
[array([[1, 2, 3, 4]]), array([[5, 6, 7, 8]])]
>>> c = np.hsplit(a, 2)
>>> c
[array([[1, 2],
       [5, 6]]), array([[3, 4],
       [7, 8]])]
>>> np.vstack(c)
array([[1, 2],
       [5, 6],
       [3, 4],
       [7, 8]])
>>> np.hstack(c)
array([[1, 2, 3, 4],
       [5, 6, 7, 8]])
```

- `b = np.copy(a)` - stvara kopiju od `a` i sprema u `b`

- `rand`:

```
>>> np.random.rand()
0.3867390663171938
>>> np.random.rand(2, 3)
array([[ 0.35582925,  0.52437692,  0.5095258 ],
       [ 0.35860294,  0.25066797,  0.72845161]])
```

## MPI4Py

- importanje:

```
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
```

- pokretanje:

```
mpirun -np 4 python3 prog.py
```

### Blokirajuća komunikacija

- čeka na razmjenu poruka

- `comm.Send(array, dest, tag)`
    - `array` numpy polje koje šaljemo
    - `dest` rang procesa koji prima poruku
    - `tag` **opcionalan** broj; ID poruke

- `comm.Recv(array, source, tag)`
    - `array` numpy polje u koje se sprema rezultat; **treba ga stvoriti unaprijed**
    - `source` rang procesa koji je poslao poruku
    - `tag` **opcionalan** broj; ID poruke

### Neblokirajuća komunikacija

- ne čeka na razmjenu poruka, izvođenje se nastavlja

- `request = comm.Isend(array, dest, tag)`
    - `array`, `dest`, `tag` isti kao i kod funkcije `Send()`

- `request = comm.Irecv(array, source, tag)`
    - `array`, `source`, `tag` isti kao i kod funkcije `Recv()`

- `request.Wait()`
    - čeka na izvršenje zahtjeva za slanjem ili primanjem

- `MPI.Request.Waitall([request1, request2])`
    - čeka na izvršenje zahtjeva `request1` i `request2`; kraći zapis kod velikog broja zahtjeva

### Kolektivna komunikacija

- sljedeće funkcije se moraju pozvati na svim procesima, ne samo na korijenskom

- `comm.Barrier()` - sinkronizacija korištenjem barijere; čeka se da svi procesi dođu do barijere

![bcast](http://i.imgur.com/1zmYyzI.png)

- `comm.Bcast(array, root)` - vrijednost sa korijenskog procesa šalje svima; podatak s njega bit će **prepisan** preko podataka na ostalim procesima
    - `array` numpy polje koje se šalje; mora biti istog oblika na svim procesima
    - `root` rang korijenskog procesa

![sctter_gather](http://i.imgur.com/jdieL7Z.png)

- `comm.Scatter(sendmsg, recvmsg, root)` - listu raspršuje po procesima, svaki proces dobiva po **jedan** element i to točno onaj s indeksom koliki je njegov rang
    - `sendmsg` numpy polje koje se raspršuje; mora biti **iste veličine kao broj procesa** na korijenskom procesu, i `None` na ostalima
    - `recvmsg` numpy polje veličine jednog elementa u koje se sprema rezultat raspršenja
    - `root` rang korijenskog procesa

- `comm.Gather(sendmsg, recvmsg, root)` - vrši obrnut proces od `Scatter()`, po jednu varijablu sa svakog procesa skuplja u polje na korijenskom procesu (vrijednost se postavlja na indeks koliki je rang pripadnog procesa)
    - `sendmsg` numpy polje koje se šalje korijenskom procesu
    - `recvmsg` numpy polje u koje se sakupljaju primljene vrijednosti; ono mora biti veličine koliki je broj procesa na korijenskom procesu, i `None` na ostalima
    - `root` rang korijenskog procesa

![allgather](http://i.imgur.com/Ocacipu.png)

- `comm.Allgather(sendmsg, recvmsg)` - vrši operaciju kao i `Gather()`, osim što sada rezultirajuću listu dobivaju svi procesi, a ne samo korijenski
    - `sendmsg` numpy polje koje se šalje
    - `recvmsg` numpy polje u koje se sakupljaju primljene vrijednosti

- `comm.Reduce(sendmsg, recvmsg, op, root)` - vrši proces redukcije (maksimum, minimum, suma, produkt) na danom skupu elemenata koji čini po jedna vrijednost sa svakog od procesa i rezultat redukcije sprema u varijablu na korijenskom procesu
    - `sendmsg` numpy polje na kojem se vrši redukcija
    - `recvmsg` numpy polje u koje se sprema rezultat; ono mora biti odgovarajuće veličine na korijenskom procesu, i `None` na ostalima
    - `op` operator redukcije; `MAX`, `MIN`, `SUM`, `PROD`...
    - `root` rang korijenskog procesa

- `comm.Allreduce(sendmsg, recvmsg, op)` - vrši istu operaciju kao `Reduce()`, osim što sada rezultat dobivaju svi procesi, a ne samo korijenski
    - `sendmsg` numpy polje na temelju kojeg se vrši redukcija
    - `recvmsg` numpy polje u koje se sprema rezultat
    - `op` operator redukcije

![alltoall](http://i.imgur.com/UfyuY15.png)

- `comm.Alltoall(sendmsg, recvmsg)` - element vektora sa indeksom *j* iz `sendmsg` na procesu *i* postaje element vektora sa indeksom *i* u `recvmsg` na procesu *j*
    - `sendmsg` numpy polje koje se šalje, veličine jednake ukupnom broju procesa
    - `recvmsg` je numpy polje koje se prima, veličine jednake ukupnom broju procesa

### Komunikatori i grupe procesa

- `comm = MPI.COMM_WORLD` - komunikator koji uključuje sve pokrenute procese

- `MPI.COMM_NULL` - prazan komunikator

- `group = comm.Get_group()` - metoda komunikatora koja dohvaća grupu

- `newgroup = group.Excl(ranks)` - iz grupe isključuje procese s rangovima u listi `ranks`

- `newcomm = comm.Create(newgroup)` - novi komunikator zasnovan na grupi `newgroup`

- `group.Free()` - prazni grupu

- `newcomm = MPI.COMM_WORLD.Split(color, key)` - dobivamo dvije podgrupe procesa koje ne komuniciraju međusobno
    - `color` je cijeli broj koji određuje raspored procesa po podgrupama
    - `key` je cijeli broj koji određuje kojim redom će se dodijeliti rang procesima u novonastalim podgrupama