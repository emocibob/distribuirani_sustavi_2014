"""
 * Modficirajte program tako da poruke razmjenjuju tri procesa, i to tako da proces
   0 šalje podatak tipa po vašoj želji i vrijednosti po vašoj želji procesu 1, proces
   1 šalje isti podatak procesu 2, a proces 2 podatak šalje procesu 0.
"""
# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 3:
        print("Potrebna su barem tri procesa za izvođenje")
        exit()

if rank == 0:
        sendmsg = np.array([1, 2, 3], dtype=np.int32)
        comm.Send(sendmsg, dest=1)

        recvmsg = np.zeros((3), dtype=np.int32)
        comm.Recv(recvmsg, source=2)
        
elif rank == 1:
        recvmsg = np.zeros((3), dtype=np.int32)
        comm.Recv(recvmsg, source=0)

        sendmsg = recvmsg
        comm.Send(sendmsg, dest=2)
        
elif rank == 2:
        recvmsg = np.zeros((3), dtype=np.int32)
        comm.Recv(recvmsg, source=1)

        sendmsg = recvmsg
        comm.Send(sendmsg, dest=0)

else:
        print("Proces ranga", rank, "ne razmjenjuje poruke")
        exit()

print("Proces ranga", rank, "poslao je poruku", sendmsg)
print("Proces ranga", rank, "primio je poruku", recvmsg)
