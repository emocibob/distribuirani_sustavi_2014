"""
 * Napišite program koji koristi MPI za izračun zbroja kubova brojeva u rasponu od 1 do
   300000 u 6 procesa korištenjem komunikacije točka-do točke. Raspon raspodijelite po
   procesima po želji. Proces ranga 0 je onaj kojem će preostalih pet procesa javiti svoje
   rezultate i koji će rezultate sumirati te ispisati na ekran. Ostali procesi neka ne
   ispisuju ništa. Kod slanja iskoristite neblokirajuću komunikaciju.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if size < 6:
    print("Potrebno barem 6 procesa.")
    exit()

if rank == 0:
    a = np.array([x for x in range(1, 5001)], dtype=np.int64) # 0
    b = np.array([x for x in range(5001, 10001)], dtype=np.int64) # 0
    c = np.array([x for x in range(10001, 15001)], dtype=np.int64) # 0
    d = np.array([x for x in range(15001, 20001)], dtype=np.int64) # 0
    e = np.array([x for x in range(20001, 25001)], dtype=np.int64) # 0
    f = np.array([x for x in range(25001, 30001)], dtype=np.int64) # 0

    zbroj = np.zeros(1, dtype=np.int64)

    for n in a:
        zbroj += n**3

    r1 = comm.Isend(b, dest=1)
    r1.Wait()
    r2 = comm.Isend(c, dest=2)
    r2.Wait()
    r3 = comm.Isend(d, dest=3)
    r3.Wait()
    r4 = comm.Isend(e, dest=4)
    r4.Wait()
    r5 = comm.Isend(f, dest=5)
    r5.Wait()
        
    for i in range(1, 6):
        temp = np.zeros(1, dtype=np.int64)
        r = comm.Irecv(temp, source=i)
        r.Wait()
        print(rank, "primio od", i)
        zbroj += temp

    print("Zbroj kubova:", zbroj)

elif rank in [1, 2, 3, 4, 5]:
    x = np.zeros(5000, dtype=np.int64) # 0
    suma = np.zeros(1, dtype=np.int64)
    
    r = comm.Irecv(x, source=0)
    r.Wait()
    print(rank, "primio od 0")
    
    for n in x:
        suma += n**3

    r2 = comm.Isend(suma, dest=0)
    r2.Wait()
    print(rank, "posalo 0")
    
else:
    exit()
