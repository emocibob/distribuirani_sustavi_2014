"""
 * Promijenite kod da su vektori veličine 6 elemenata umjesto 4, i izvedite kod u 6 procesa.

 * Dodajte još jedan vektor veličine 6 elemenata i izračunajte zbroj tri vektora umjesto
   zbroja dva vektora.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if size < 6:
    print("Potrebno barem 6 procesa.")
    exit()

if rank == 0:
    a = np.array([1, 2, 3, 9, 10, 20], dtype=np.float32)
    b = np.array([4, 5, 6, 7, 30, 40], dtype=np.float32)
    c = np.array([100, 200, 300, 400, 500, 600], dtype=np.float32)
    zbroj = np.zeros(6, dtype=np.float32)
    zbroj[0] = a[0] + b[0] + c[0]

    comm.Send(a[1], dest=1, tag=1)
    comm.Send(b[1], dest=1, tag=2)
    comm.Send(c[1], dest=1, tag=3)
    comm.Send(a[2], dest=2, tag=1)
    comm.Send(b[2], dest=2, tag=2)
    comm.Send(c[2], dest=2, tag=3)
    comm.Send(a[3], dest=3, tag=1)
    comm.Send(b[3], dest=3, tag=2)
    comm.Send(c[3], dest=3, tag=3)
    comm.Send(a[4], dest=4, tag=1)
    comm.Send(b[4], dest=4, tag=2)
    comm.Send(c[4], dest=4, tag=3)
    comm.Send(a[5], dest=5, tag=1)
    comm.Send(b[5], dest=5, tag=2)
    comm.Send(c[5], dest=5, tag=3)
    
    suma_temp = np.zeros(1, dtype=np.float32)

    comm.Recv(suma_temp, source=1)
    zbroj[1] = suma_temp
    comm.Recv(suma_temp, source=2)
    zbroj[2] = suma_temp
    comm.Recv(suma_temp, source=3)
    zbroj[3] = suma_temp
    comm.Recv(suma_temp, source=4)
    zbroj[4] = suma_temp
    comm.Recv(suma_temp, source=5)
    zbroj[5] = suma_temp
    
    print("a =", a)
    print("b =", b)
    print("c =", c)
    print("Zbroj je", zbroj)

elif rank == 1:
    el_a = np.zeros(1, dtype=np.float32)
    el_b = np.zeros(1, dtype=np.float32)
    el_c = np.zeros(1, dtype=np.float32)
    
    comm.Recv(el_a, source=0, tag=1)
    comm.Recv(el_b, source=0, tag=2)
    comm.Recv(el_c, source=0, tag=3)

    el_zbroj = el_a + el_b + el_c

    comm.Send(el_zbroj, dest=0)

elif rank == 2:
    el_a = np.zeros(1, dtype=np.float32)
    el_b = np.zeros(1, dtype=np.float32)
    el_c = np.zeros(1, dtype=np.float32)
    
    comm.Recv(el_a, source=0, tag=1)
    comm.Recv(el_b, source=0, tag=2)
    comm.Recv(el_c, source=0, tag=3)

    el_zbroj = el_a + el_b + el_c

    comm.Send(el_zbroj, dest=0)

elif rank == 3:
    el_a = np.zeros(1, dtype=np.float32)
    el_b = np.zeros(1, dtype=np.float32)
    el_c = np.zeros(1, dtype=np.float32)
    
    comm.Recv(el_a, source=0, tag=1)
    comm.Recv(el_b, source=0, tag=2)
    comm.Recv(el_c, source=0, tag=3)

    el_zbroj = el_a + el_b + el_c

    comm.Send(el_zbroj, dest=0)

elif rank == 4:
    el_a = np.zeros(1, dtype=np.float32)
    el_b = np.zeros(1, dtype=np.float32)
    el_c = np.zeros(1, dtype=np.float32)
    
    comm.Recv(el_a, source=0, tag=1)
    comm.Recv(el_b, source=0, tag=2)
    comm.Recv(el_c, source=0, tag=3)

    el_zbroj = el_a + el_b + el_c

    comm.Send(el_zbroj, dest=0)

elif rank == 5:
    el_a = np.zeros(1, dtype=np.float32)
    el_b = np.zeros(1, dtype=np.float32)
    el_c = np.zeros(1, dtype=np.float32)
    
    comm.Recv(el_a, source=0, tag=1)
    comm.Recv(el_b, source=0, tag=2)
    comm.Recv(el_c, source=0, tag=3)

    el_zbroj = el_a + el_b + el_c

    comm.Send(el_zbroj, dest=0)
    
else:
    exit()
