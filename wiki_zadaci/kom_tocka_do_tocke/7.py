"""
 * Napišite program koji koristi MPI za izračun zbroja kvadrata brojeva u rasponu od 1 do
   500000 u 3 procesa korištenjem komunikacije točka-do točke, i to tako da proces ranga 0
   šalje procesu ranga 1 i procesu ranga 2 liste koje sadrže brojeve od 1 do 250000 i od
   250000 do 500000 (respektivno). Procesi ranga 1 i 2 računaju zbroj kvadrata brojeva koje
   su dobili. Procesu ranga 0 procesi ranga 1 i 2 javljaju rezultate koje su dobili. Proces
   ranga 0 prima oba rezultata i njihov zbroj ispisuje na ekran. Iskoristite blokirajuću
   komunikaciju.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if size < 3:
    print("Potrebno barem 3 procesa.")
    exit()

if rank == 0:
    a = np.array([x for x in range(1, 250001)], dtype=np.int64)
    b = np.array([x for x in range(250001, 500001)], dtype=np.int64)

    r1 = comm.Send(a, dest=1)
    r2 = comm.Send(b, dest=2)

    zbroj_1 = np.zeros(1, dtype=np.int64)
    zbroj_2 = np.zeros(1, dtype=np.int64)

    r3 = comm.Recv(zbroj_1, source=1)
    r4 = comm.Recv(zbroj_2, source=2)
    
    zbroj = zbroj_1 + zbroj_2

    print("Zbroj kvadrata:", zbroj)
    
elif rank == 1 or rank == 2:
    zbroj = np.zeros(1, dtype=np.int64)
    n = np.zeros(250000, dtype=np.int64)

    r1 = comm.Recv(n, source=0)

    for x in n:
        zbroj += x**2

    r2 = comm.Send(zbroj, dest=0)

else:
    exit()
