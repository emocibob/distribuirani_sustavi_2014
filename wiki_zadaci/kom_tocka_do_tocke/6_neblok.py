"""
 * Napišite dvije varijante ovog programa. Prva varijanta neka koristi blokirajuću
   komunikaciju, a druga neblokirajuću. Obje varijante izvode se u četiri procesa.

   * Na procesu ranga 0 incijaliziraju se dva vektora veličine 6 elemenata. Po dva
     elementa svakog vektora šalju se procesima ranga 1, 2 i 3.

   * Procesi ranga 1, 2 i 3 vrše zbroj odgovarajućih elemenata vektora i rezultat šalju
     procesu ranga 0.

   * Proces ranga 0 prima rezultate od procesa ranga 1, 2 i 3 i vrši provjeru točnosti
     rješenja.
"""
# neblokirajuća komunikacija
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if size < 4:
    print("Potrebno je barem 4 procesa.")
    exit()

if rank == 0:
    a = np.array([1.0, 2.0, 3.0, 9.0, 10.0, 12.0])
    b = np.array([4.0, 5.0, 6.0, 7.0, 11.0, 20.0])
    zbroj = np.empty(6)

    for i in range(0, 5, 2):
        r1 = comm.Isend(a[i:i+2], dest=(i+2)/2, tag=0)
        r2 = comm.Isend(b[i:i+2], dest=(i+2)/2, tag=1)
        MPI.Request.Waitall([r1, r2])
        
    for i in range(0, 5, 2):
        r = comm.Irecv(zbroj[i:i+2], source=(i+2)/2)
        r.Wait()
        
    print("a =", a)
    print("b =", b)
    print("Zbroj je", zbroj)

    zbroj_provjera = a + b
    print("Prvojera prolazi:", zbroj == zbroj_provjera)
        
elif rank in [1, 2, 3]:
    dio_a = np.empty(2)
    dio_b = np.empty(2)

    r1 = comm.Irecv(dio_a, source=0, tag=0)
    r2 = comm.Irecv(dio_b, source=0, tag=1)
    MPI.Request.Waitall([r1, r2])
    
    zbroj = dio_a + dio_b

    r = comm.Isend(zbroj, dest=0)
    r.Wait()

else:
    exit()
