"""
 * Napišite program koji se izvodi u tri procesa i koristi blokirajuću komunikaciju:

   * proces ranga 1 računa zbroj i produkt parnih prirodnih brojeva manjih ili jednakih
     10 i rezultat šalje procesu ranga 0 kao listu koja sadrži dva elementa tipa int
     ili numpy polje od dva elementa tipa numpy.int32,

   * proces ranga 2 računa zbroj i produkt prirodnih brojeva manjih ili jednakih 20 i
     rezultat šalje procesu ranga 0 kao listu koja sadrži dva elementa tipa int ili kao
     numpy polje od dva elementa tipa numpy.int64,

   * proces ranga 0 prima rezultate i ispisuje ih na ekran.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 3:
    print("Potrebno barem 3 procesa.")
    exit()

if rank == 0:
    recvmsg = np.zeros((2), dtype=np.int32)
    comm.Recv(recvmsg, source=1)
    print("Ja sam proces", rank, "i od procsa 1 primio sam:", recvmsg)

    recvmsg = np.zeros((2), dtype=np.int64)
    comm.Recv(recvmsg, source=2)
    print("Ja sam proces", rank, "i od procsa 2 primio sam:", recvmsg)
    
elif rank == 1:
    suma = 0
    prod = 1

    for i in range(1, 11):
        if i % 2 == 0:
            suma += i
            prod *= i

    sendmsg = np.array([suma, prod], dtype=np.int32)
    comm.Send(sendmsg, dest=0)

elif rank == 2:
    suma = 0
    prod = 1

    for i in range(1, 21):
        suma += i
        prod *= i

    sendmsg = np.array([suma, prod], dtype=np.int64)
    comm.Send(sendmsg, dest=0)
    
else:
    exit()
