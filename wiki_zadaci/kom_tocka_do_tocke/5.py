"""
 * Ponovno promijenite kod da su vektori veličine 6 elemenata umjesto 4, i izvedite kod u 6
   procesa.

 * Dodajte još jedan vektor veličine 6 elemenata i izračunajte zbroj tri vektora umjesto 
   zbroja dva vektora.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if size < 6:
    print("Potrebno barem 6 procesa.")
    exit()

if rank == 0:
    a = np.array([1, 2, 3, 9, 10, 20], dtype=np.float32)
    b = np.array([4, 5, 6, 7, 30, 40], dtype=np.float32)
    c = np.array([100, 200, 300, 400, 500, 600], dtype=np.float32)
    zbroj = np.zeros(6, dtype=np.float32)
    zbroj[0] = a[0] + b[0] + c[0]

    for i in range(1, 6):
        comm.Send(a[i], dest=i, tag=1)
        comm.Send(b[i], dest=i, tag=2)
        comm.Send(c[i], dest=i, tag=3)

    temp = np.zeros(1, dtype=np.float32)
    for i in range(1, 6):
        comm.Recv(temp, source=i)
        zbroj[i] = temp

    print("a =", a)
    print("b =", b)
    print("c =", c)
    print("Zbroj je", zbroj)

elif rank in [1, 2, 3, 4, 5]:
    el_a = np.zeros(1, dtype=np.float32)
    el_b = np.zeros(1, dtype=np.float32)
    el_c = np.zeros(1, dtype=np.float32)

    comm.Recv(el_a, source=0, tag=1)
    comm.Recv(el_b, source=0, tag=2)
    comm.Recv(el_c, source=0, tag=3)

    el_zbroj = el_a + el_b + el_c

    comm.Send(el_zbroj, dest=0)

else:
    exit()
