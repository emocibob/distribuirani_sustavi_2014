"""
 * Napišite program koji koristi MPI za izračun zbroja kubova brojeva u rasponu od 1
   do 300000 u 6 procesa korištenjem kolektivne komunikacije tipa scatter-reduce. Na
   procesu ranga 0 inicijalizirajte listu pojedinih raspona i raspodijelite je procesima
   koji kubiraju dobivene brojeve i zbrajaju ih. Nakon završetka obrade na procesu ranga
   0 izvedite redukciju sumiranjem i na procesu ranga 0 ispišite rezultat na ekran. Ostali
   procesi neka ne ispisuju ništa.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    raspon = np.array([np.arange(1, 5001), # 0
                       np.arange(5001, 10001), # 0
                       np.arange(10001, 15001), # 0
                       np.arange(15001, 20001), # 0
                       np.arange(20001, 25001), # 0
                       np.arange(25001, 30001)], dtype=np.int64) # 0
    krajnja_suma = np.zeros(1, dtype=np.int64)
else:
    raspon = None
    krajnja_suma = None

dio = np.zeros(50000, dtype=np.int64) # 0
comm.Scatter(raspon, dio, root=0)

suma = 0
for a in dio:
    suma += a**3

comm.Reduce(suma, krajnja_suma, op=MPI.SUM, root=0)
if rank == 0:
    print("Zbroj kubova:", krajnja_suma)
