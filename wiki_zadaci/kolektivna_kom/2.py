"""
 * Modificirajte kod tako da se izvodi u 5 procesa, a scatter se vrši nad matricom
   proizvoljnih vrijednosti koja ima 5 redaka i 4 stupca zapisanom u obliku liste, odnosno
   numpy polja.

 * Učinite da svaki od procesa posljednji element u primljenom retku matrice postavlja na
   vrijednost 0, a zatim ispisuje taj redak na ekran.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size != 5:
    print("Potrebno 5 procesa.")
    exit()

if rank == 0:
    sendmsg = np.arange(20, dtype=np.int32)
    sendmsg = np.reshape(sendmsg, (5, 4))
else:
    sendmsg = None

recvmsg = np.zeros(4, dtype=np.int32)
comm.Scatter(sendmsg, recvmsg, root=0)
print("Proces ranga", rank, "ima vrijednost poruke za slanje", sendmsg, ", primljena poruka", recvmsg)

recvmsg[-1] = 0
print("Proces ranga", rank, "novi redak", recvmsg)
