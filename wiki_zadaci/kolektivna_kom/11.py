"""
 * Modificirajte primjer dan za Monte Carlo simulaciju da dodate kod koji mjeri vrijeme
   izvođenja za svaki od procesa.

 * Usporedite vrijeme izvođenja simulacije za 2, 3, 4 procesa kad svaki proces izvodi
   10^4^, 10^5^, 10^6^ iteracija. Opišite svoje zaključke.
"""
