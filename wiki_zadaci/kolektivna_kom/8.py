"""
 * Dodajte kod za još dvije redukcije:
   
   * prva neka nalazi najmanju vrijednost među dobivenim produktima,

   * druga neka nalazi najveću vrijednost među dobivenim produktima.

 * (Uputa: op=MPI.MIN i op=MPI.MAX)
"""
# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = np.array([1, 2, 3, 9, 10, 11], dtype=np.float32)
    b = np.array([4, 5, 6, 7, 8, 24], dtype=np.float32)
    min_el = np.zeros(1, dtype=np.float32)
    max_el = np.zeros(1, dtype=np.float32) 
else:
    a = None
    b = None
    min_el = None
    max_el = None

element_a = np.empty(1, dtype=np.float32)
comm.Scatter(a, element_a, root=0)
element_b = np.empty(1, dtype=np.float32)
comm.Scatter(b, element_b, root=0)
produkt_elemenata = element_a * element_b

skalarni_produkt = np.empty(1, dtype=np.float32)
comm.Reduce(produkt_elemenata, skalarni_produkt, op=MPI.SUM, root=0)
if rank == 0:
    print("Skalarni produkt je", skalarni_produkt)

comm.Reduce(produkt_elemenata, min_el, op=MPI.MIN, root=0)
comm.Reduce(produkt_elemenata, max_el, op=MPI.MAX, root=0)
if rank == 0:
    print("Minimalni produkt:", min_el)
    print("Maksimalni produkt:", max_el)
