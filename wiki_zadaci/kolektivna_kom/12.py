"""
 * Usporedite vrijeme izvođenja programa i točnost aproksimacije pi korištenjem:

   * sumiranja elemenata niza,

   * Monte Carlo simulacije,

 * za 2, 3, 4 procesa kad svaki proces izvodi 10^4^, 10^5^, 10^6^ iteracija. Opišite
   svoje zaključke.
"""
