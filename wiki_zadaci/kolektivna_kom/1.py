"""
 * Prilagodite kod primjera tako da se broadcastom šalje lista s 4 podliste s po 4 elementa,
   odnosno polje oblika (4, 4), u oba slučaja s proizvoljnim vrijednostima.

 * Učinite da nakon broadcasta proces ranga 2 promijeni vrijednost elementa na poziciji (1, 1)
   tako da je uveća za 5, i zatim napravite broadcast s njega svim ostalima.

 * Inicijalizirajte na procesu ranga 1 polje formata (5, 15) kodom np.array([list(range(x, x +
   15)) for x in range(5)]) i napravite broadcast s njega svim ostalima (pripazite na tip
   polja u koje primate). Učinite da svaki od procesa po primitku polja ispisuje na ekran
   vrijednost na koordinati (rang, rang).

 Na svim procesima ispišite primljene podatke na ekran da provjerite je li operacija bila
 uspješna.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    msg = np.array([[1.5, 2.5, 3.5, 4.5],
                    [5.5, 6.5, 7.5, 8.5],
                    [9.5, 10.5, 11.5, 12.5],
                    [13.5, 14.5, 15.5, 16.5]])
else:
    msg = np.zeros((4, 4))

print("Prije broadcasta proces ranga", rank, "ima vrijednost varijable", msg)
comm.Bcast(msg, root=0)
print("Nakon broadcasta proces ranga", rank, "ima vrijednost varijable", msg)

if rank == 2:
    msg[1][1] += 5

comm.Bcast(msg, root=2)

print("Nakon 2. broadcasta proces ranga", rank, "ima vrijednost varijable", msg)

if rank == 1:
    polje = np.array([list(range(x, x + 15)) for x in range(5)], dtype=np.int32)
else:
    polje = np.zeros((5, 15), dtype=np.int32)

comm.Bcast(polje, root=1)

print("Nakon 3. broadcasta proces ranga", rank, "ima vrijednost 'polje'", polje)
print(rank, ",", rank, "=>", polje[rank][rank])
