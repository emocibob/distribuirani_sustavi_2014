"""
 * Učinite da program umjesto zbroja vektora računa produkt vektora po elementima, odnosno
   vektor u kojem je svaki element produkt elemenata u vektorima s pripadnim indeksom.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    a = np.array([1, 2, 3, 9, 10, 20], dtype=np.int32)
    b = np.array([4, 5, 6, 7, 50, 70], dtype=np.int32)
    c = np.array([100, 200, 300, 400, 500, 600], dtype=np.int32)
    produkt = np.zeros(6, dtype=np.int32)
else:
    a = None
    b = None
    c = None
    produkt = None

el_a = np.zeros(1, dtype=np.int32)
el_b = np.zeros(1, dtype=np.int32)
el_c = np.zeros(1, dtype=np.int32)
comm.Scatter(a, el_a, root=0)
comm.Scatter(b, el_b, root=0)
comm.Scatter(c, el_c, root=0)

produkt_dio = el_a * el_b * el_c
comm.Gather(produkt_dio, produkt, root=0)
if rank == 0:
    print("Produkt vektora:", produkt)
    test_produkt = np.multiply(a, b)
    test_produkt = np.multiply(test_produkt, c)
    print("Test:", produkt == test_produkt)
