"""
 * Napišite program koji koristi MPI za izračun zbroja kvadrata brojeva u rasponu od 1
   do 500000 u 4 procesa korištenjem kolektivne komunikacije tipa scatter-reduce.
   Raspodijelite po želji; za to morate napraviti listu oblika

   brojevi = [[1, 2, 3, ..., 125000],
              [125001, 125002, ..., 250000],
              [250001, 250002, ..., 375000],
              [375001, 375002, ..., 500000]]

 * odnosno potrebno je da ima 4 podliste od kojih će savka biti dana odgovarajućem
   procesu. Svi procesi računaju zbroj kvadrata brojeva koje su dobili. Nakon završetka
   obrade na procesu ranga 0 sakupite rezultate i to tako da izvršite redukciju
   korištenjem sumiranja.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    brojevi = np.array([np.arange(1, 125001),
                        np.arange(125001, 250001),
                        np.arange(250001, 375001),
                        np.arange(375001, 500001)], dtype=np.int64)
    recvmsg = np.zeros(125000, dtype=np.int64)
    suma = np.zeros(1, dtype=np.int64)
else:
    brojevi = None
    suma = None
    
recvmsg = np.zeros(125000, dtype=np.int64)
comm.Scatter(brojevi, recvmsg, root=0)

suma_kv = 0
for a in recvmsg:
    suma_kv += a**2

comm.Reduce(suma_kv, suma, op=MPI.SUM, root=0)
if rank == 0:
    print("Suma kvadrata:", suma)
