"""
 * Promijenite kod tako da:

   * na glavnom procesu se inicijalizira lista ili polje slučajnih vrijednosti, koje se
     raspršuju (scatter) na sve procese,

   * svaki od procesa kvadrira vrijednost koju primi,

   * zatim se vrši redukcija na proces ranga 0 operacijom MIN i redukcija na sve procese
     operacijom MAX.

 * Neka se takav program izvodi u 8 procesa. Alternativno, napravite da se može izvoditi u
   proizvoljnom broju procesa.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
        sendmsg = np.array([np.random.random() for i in range(size)], dtype=np.float32)
        min_kv = np.zeros(1, dtype=np.float32)
else:
        sendmsg = None
        min_kv = None
        
recvmsg = np.zeros(1, dtype=np.float32)
comm.Scatter(sendmsg, recvmsg, root=0)

print(rank, sendmsg, recvmsg)

kv = recvmsg**2
comm.Reduce(kv, min_kv, op=MPI.MIN, root=0)

max_kv = np.zeros(1, dtype=np.float32)
comm.Allreduce(kv, max_kv, op=MPI.MAX)

if rank == 0:
        print("Ja sam proces", rank, ", min je:", min_kv, ", max je:", max_kv)
