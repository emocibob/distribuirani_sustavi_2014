"""
 * Stvorite jednodimenzionalno polje a proizvoljnih cijelobrojnih vrijednosti veličine 10
   i listu i = [1,3,5,9] te isprobajte iduće naredbe:

   a[i]
   a[i][2]
   a[i][1:3]
   a[i][:-1]
   a[i] * a[i][1]
   a[i]**2 + i
   a[i] / i[1]
   np.sin(a[i]) * np.cos(i[2])
"""
import numpy as np

a = np.array([x for x in range(10, 110, 10)])
i = [1, 3, 5, 9]
print(a)
print(i, "\n")

print("a[i] =", a[i])
print("a[i][2] =", a[i][2])
print("a[i][1:3] =", a[i][1:3])
print("a[i][:-1] =", a[i][:-1])
print("a[i] * a[i][1] =", a[i] * a[i][1])
print("a[i]**2 + i =", a[i]**2 + i)
print("a[i] / i[1] =", a[i] / i[1])
print("np.sin(a[i]) * np.cos(i[2]) =", np.sin(a[i]) * np.cos(i[2]))
