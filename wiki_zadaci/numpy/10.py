"""
 * Stvorite višedimenzionalno polje a proizvoljnih cijelobrojnih vrijednosti oblika (5,4).
   Učinite iduće:

   * Trajno promijenite oblik polja u (2,10).

   * Transponirajte polje uz povećavanje svih vrijednosti polja za kosinus od 5.

   * Stvorite jednodimenzionalno polje b preoblikovanjem polja a tako da su vrijednosti
     elementa u b dvostruko veće od vrijednosti elemenata iz a.
"""
import numpy as np

a = np.array([[y + x for x in range(1, 5)] for y in range(0, 17, 4)])
print(a, "\n")

a = np.resize(a, (2, 10))
print(a)

a = np.transpose(a) + np.cos(5)
print(a)

b = np.ravel(a) * 2
print(b)
