"""
 * Stvorite dvije datoteke, nazovite ih matrica_a.txt i matrica_b.txt. Matrica u prvoj
   datoteci neka bude oblika (3, 5), a u drugoj datoteci oblika (5, 4).

 * Izvršite čitanje podataka, a zatim izračunajte produkt dvaju matrica. Možete li
   izračunati oba produkta ili samo jedan? Objasnite zašto.
"""
import numpy as np

s1 = open('matrica_a.txt')
a = np.loadtxt(s1)
s1.close()

s2 = open('matrica_b.txt')
b = np.loadtxt(s2)
s2.close()

print(a)
print(b)
print("\n", np.dot(a, b))
