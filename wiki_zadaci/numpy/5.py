"""
 * Stvorite polje u kojem su sve vrijednosti jednake 9.45 tipa float64 i pretvorite ga
   u polje tipa float32 i rezultat spremite u novo polje. Uočavate li gubitak preciznosti?

 * Pretvorite dobiveno polje tipa float32 u polje tipa float64. Je li rezultat jednak
   početnom polju?

 * Iskoristite round da na rezultirajućem polju tipa float64 dobijete iste vrijednosti
   kao na početnom. 
 
 Napomena: rezultat ovog zadatka uvelike ovisi o računalu na kojem radite.
"""
import numpy as np

a = np.array([[9.45 for i in range(5)] for j in range(4)], dtype=np.float64) # generiraj 4x5 matricu
b = a.astype(np.float32)
print(a)
print(b)
print(a == b)

b = b.astype(np.float64)
print("\n", b)

b = np.round(b, 2)
print("\n", b)
print(a == b)
