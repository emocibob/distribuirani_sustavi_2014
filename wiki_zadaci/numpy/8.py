"""
 * Stvorite višedimenzionalno polje a proizvoljnih cijelobrojnih vrijednosti oblika (5,4)
   te isprobajte iduće naredbe:

   a[2]
   a[-1]
   a[2:3]
   a[0:4, 2]
   a[::-2]
   b[1:3, : ]
"""
import numpy as np


a = np.array([[y * x for x in range(10, 50, 10)] for y in range(5, 11)])
print(a, "\n")

print("a[2] =", a[2])
print("a[-1] =", a[-1])
print("a[0:4, 2] =", a[0:4, 2])
print("a[1:3, : ] =", a[1:3, : ])
