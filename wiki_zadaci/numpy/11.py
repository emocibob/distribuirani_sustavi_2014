"""
 * Stvorite polje a oblika (2,4) proizvoljnih cjelobrojnih vrijednosti.

   * Funkcijom za horizontalno razdvajanje razdvojite a na dva jednaka dijela te
     rezultat spremite u b. Kojeg tipa podataka je b. Što sadrži?

   * Nad b iskoristite funkcije za horizontalno te za vertikalno spajanje polja pa
     zatim usporedite dobivene rezultate. Kojeg su oblika dobivena polja?
"""
import numpy as np

a = np.reshape(np.arange(1, 9), (2, 4))
print(a, "\n")

b = np.hsplit(a, 2)
print(b)
print(type(b), "\n")

c = np.vstack(b)
d = np.hstack(b)
print(c, c.shape)
print(d, d.shape)
