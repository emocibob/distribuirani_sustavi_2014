"""
 * Stvorite jednodimenzionalno polje a proizvoljnih cijelobrojnih vrijednosti veličine
   10 te funkcijom stvorite objekt a_kopija koji je kopija objekta a.

 * Promjenite proizvoljnu vrijednost polja a_copy te nakon toga promijenite oblik
   proizvoljnom funkcijom za mijenjanje oblika.

 * Usporedite sadržaj i oblik polja a i a_copy. Što možete zaključiti? Je li promjena
   vrijednosti objekta a_copy utjecala na vrijednosti objekta a?
"""
import numpy as np

a = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=np.int32)
print(a, "\n")

a_kopija = np.copy(a)
a_kopija[0] = 0
a_kopija.shape = 5, 2

print(a)
print(a_kopija)
