"""
 * Stvorite grupu procesa koja sadrži samo procese iz grupe COMM_WORLD koji imaju parni
   rang. Iskoristite novu grupu da bi stvorili novi komunkator. (Uputa: iskoristite
   Group.Incl(), Group.Range_incl() ili Group.Excl().)
"""
from mpi4py import MPI

comm = MPI.COMM_WORLD
group = comm.Get_group()
size = comm.Get_size()

newgroup = group.Excl([x for x in range(size) if x % 2 != 0])
newcomm = comm.Create(newgroup)

group.Free()
newgroup.Free()

if newcomm:
    newcomm.Free()
