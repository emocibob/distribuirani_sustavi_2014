# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = np.array([1, 2, 3, 9], dtype=np.float32)
    b = np.array([4, 5, 6, 7], dtype=np.float32)
    zbroj = np.zeros(4, dtype=np.float32)
    zbroj[0:1] = a[0:1] + b[0:1]
    comm.Send(a[1:2], dest=1, tag=0)
    comm.Send(b[1:2], dest=1, tag=1)
    comm.Send(a[2:3], dest=2, tag=0)
    comm.Send(b[2:3], dest=2, tag=1)
    comm.Send(a[3:4], dest=3, tag=0)
    comm.Send(b[3:4], dest=3, tag=1)

    comm.Recv(zbroj[1:2], source=1)
    comm.Recv(zbroj[2:3], source=2)
    comm.Recv(zbroj[3:4], source=3)

    print("Zbroj je", zbroj)

elif rank == 1:
    element_a = np.zeros(1, dtype=np.float32)
    comm.Recv(element_a, source=0, tag=0)
    element_b = np.zeros(1, dtype=np.float32)
    comm.Recv(element_b, source=0, tag=1)
    element_zbroj = element_a + element_b
    comm.Send(element_zbroj, dest=0)

elif rank == 2:
    element_a = np.zeros(1, dtype=np.float32)
    comm.Recv(element_a, source=0, tag=0)
    element_b = np.zeros(1, dtype=np.float32)
    comm.Recv(element_b, source=0, tag=1)
    element_zbroj = element_a + element_b
    comm.Send(element_zbroj, dest=0)

elif rank == 3:
    element_a = np.zeros(1, dtype=np.float32)
    comm.Recv(element_a, source=0, tag=0)
    element_b = np.zeros(1, dtype=np.float32)
    comm.Recv(element_b, source=0, tag=1)
    element_zbroj = element_a + element_b
    comm.Send(element_zbroj, dest=0)

else:
    exit()
