# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = np.array([1, 2, 3, 9], dtype=np.float32)
    b = np.array([4, 5, 6, 7], dtype=np.float32)
    zbroj = np.zeros(4, dtype=np.float32)
    zbroj[0] = a[0] + b[0]
    for i in range(1, 4):
        comm.Send(a[i], dest=i, tag=0)
        comm.Send(b[i], dest=i, tag=1)
    #comm.Send(a[1], dest=1, tag=0)
    #comm.Send(b[1], dest=1, tag=1)
    #comm.Send(a[2], dest=2, tag=0)
    #comm.Send(b[2], dest=2, tag=1)
    #comm.Send(a[3], dest=3, tag=0)
    #comm.Send(b[3], dest=3, tag=1)

    for i in range(1, 4):
        zbroj_dio = np.empty(1, dtype=np.float32)
        comm.Recv(zbroj_dio, source=i)
        zbroj[i] = zbroj_dio

    print("Zbroj je", zbroj)

elif rank in [1, 2, 3]:
    element_a = np.zeros(1, dtype=np.float32)
    comm.Recv(element_a, source=0, tag=0)
    element_b = np.zeros(1, dtype=np.float32)
    comm.Recv(element_b, source=0, tag=1)
    element_zbroj = element_a + element_b
    comm.Send(element_zbroj, dest=0)

else:
    exit()
