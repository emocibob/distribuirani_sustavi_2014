"""
 * Napišite program koji se izvodi u tri procesa i koristi
   neblokirajuću komunikaciju:

   * proces ranga 1 računa zbroj i produkt parnih prirodnih
     brojeva manjih ili jednakih 10 i rezultat šalje procesu
     ranga 0 kao listu koja sadrži dva elementa tipa int ili
     numpy polje od dva elementa tipa numpy.int32,
   
   * proces ranga 2 računa zbroj i produkt prirodnih brojeva
     manjih ili jednakih 20 i rezultat šalje procesu ranga 0
     kao listu koja sadrži dva elementa tipa int ili kao numpy
     polje od dva elementa tipa numpy.int64,
    
   * proces ranga 0 prima rezultate i ispisuje ih na ekran.
"""
# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 3:
    print("Potrebna su barem tri procesa za izvođenje")
    exit()

if rank == 0:
    recvmsg1 = np.empty(2, dtype=np.int32)
    request1 = comm.Irecv(recvmsg1, source=1)

    recvmsg2 = np.empty(2, dtype=np.int64)
    request2 = comm.Irecv(recvmsg2, source=2)

    MPI.Request.Waitall([request1, request2])

    print("Ja sam proces #" + str(rank))
    print("Od procesa #1 dobio sa:", recvmsg1)
    print("Od procesa #2 dobio sa:", recvmsg2)

elif rank == 1:
    zbroj = 0
    prod = 1
    for i in range(2, 11):
        if i % 2 == 0:
            zbroj += i
            prod *= i
    zbroj = np.int32(zbroj)
    prod = np.int32(prod)

    sendmsg = np.array([zbroj, prod], dtype=np.int32)
    request1 = comm.Isend(sendmsg, dest=0)

    request1.Wait()

elif rank == 2:
    zbroj = 0
    prod = 1
    for i in range(1, 21):
        zbroj += i
        prod *= i
    zbroj = np.int64(zbroj)
    prod = np.int64(prod)

    sendmsg = np.array([zbroj, prod], dtype=np.int64)
    request1 = comm.Isend(sendmsg, dest=0)

    request1.Wait()    

else:
    exit()
