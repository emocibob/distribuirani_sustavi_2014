# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 2:
    print("Potrebna su barem dva procesa za izvođenje")
    exit()

if rank == 0:
    sendmsg = np.array([10.0], dtype=np.float64)
    request1 = comm.Isend(sendmsg, dest=1)

    recvmsg = np.empty(1, dtype=np.int64)
    request2 = comm.Irecv(recvmsg, source=1)

    request1.Wait()
    request2.Wait()

elif rank == 1:
    recvmsg = np.empty(1, dtype=np.float64)
    request2 = comm.Irecv(recvmsg, source=0)

    sendmsg = np.array([366], dtype=np.int64)
    request1 = comm.Isend(sendmsg, dest=0)

    MPI.Request.Waitall([request1, request2])

else:
    exit()

print("Proces ranga", rank, "poslao je poruku", sendmsg)
print("Proces ranga", rank, "primio je poruku", recvmsg)
