"""
 * Stvorite jednodimenzionalno polje a
   proizvoljnih cijelobrojnih vrijednosti
   veličine 10 te isprobajte iduće naredbe:

   a[3]
   a[2:6]
   a[:8:2] = 1337
   a[[1,3,4]] = 0
   a[::-2]
   a[0] * a[2] -1
   a[[0,0,2]] = [1,2,3]
"""
import numpy as np

a = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

print("a =", a)

print("a[3] =", a[3])

print("a[2:6] =", a[2:6])

a[:8:2] = 1337
print("a[:8:2] = 1337  =>  a =", a)

a[[1,3,4]] = 0
print("a[[1,3,4]] = 0  =>  a =", a)

print("a[::-2] =", a[::-2])

print("a[0] * a[2] -1 =", a[0] * a[2] -1)

a[[0,0,2]] = [1,2,3]
print("a[[0,0,2]] = [1,2,3]  =>  a =", a)
