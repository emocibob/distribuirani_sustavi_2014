"""
 * Stvorite polje s vrijednostima

   9 13 5
   1 11 7
   3 7 2
   6 0 7

 * Saznajte mu oblik, duljinu i tip elemenata i veličinu elementa u bajtovima.

 * Stvorite polje s istim vrijednostima, ali tako da su elementi tipa float.
"""
import numpy as np

a = np.array([[9, 13, 5],
              [1, 11, 7],
              [3, 7, 2],
              [6, 0, 7]])

print("Polje:\n", a)
print("Oblik polja:", a.shape)
print("Duljina polja:", a.size)
print("Tip elemenata:", a.dtype)
print("Veličina elemenata u bajtovima:", a.itemsize)
