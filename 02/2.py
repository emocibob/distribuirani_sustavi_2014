"""
 * Stvorite polje nula oblika (5, 5) u kojem su elementi tipa numpy.float32.

 * Stvorite polje jedinica oblika (1000, 1000). Pokušajte ga ispisati naredbom
   print. Što se dogodi?
"""
import numpy as np

a = np.zeros((5, 5), dtype=np.float32)
print(a)
print(a.dtype)

b = np.ones((1000, 1000))
print("\n", b)
