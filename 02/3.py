"""
 * Stvorite dva dvodimenzionalna polja a i b oblika (3, 3)
   s proizvoljnim vrijednostima, i to tako da prvo ima elemente 
   tipa numpy.float32, a drugo elemente tipa numpy.float64.
 
 * Izračunajte 2 * a + b, cos(a), sqrt(b). Uočite kojeg su tipa
   polja koja dobivate kao rezultate.
 
 * Množenje matrica izvodite funkcijom numpy.dot(); proučite
   njenu dokumentaciju i izvedite ju na svojim poljima.
"""
import numpy as np

a = np.array([[1, 2, 3],
              [4, 5, 6],
              [7, 8, 9]], dtype=np.float32)

b = np.array([[10, 20, 30],
              [40, 50, 60],
              [70, 80, 90]], dtype=np.float64)

print("a:\n", a)
print(a.dtype)

print("\nb:\n", b)
print(b.dtype)

c = 2 * a + b
print("\n2 * a + b =\n", c)
print(c.dtype)

d = np.cos(a)
print("\ncos(a) =\n", d)
print(d.dtype)

e = np.sqrt(b)
print("\nsqrt(b) =\n", e)
print(e.dtype)

f = np.dot(a, b)
print("\nnp.dot(a, b) =\n", f)
print(f.dtype)

"""
 * Testiranje np.round
"""

g = b / 17
print("\n", g)
h = np.round(g, 3)
print("\n", h)
