import numpy as np

a = np.array([2, 3, 4])

b = a

print(a)
print(b)

a[0] = 6

print(a)
print(b)

c = a.copy()

print(c)

a[1] = 8

print(a)
print(b)
print(c)
