"""
 * Stvorite polje u kojem su sve vrijednosti jednake 9.45
   tipa float64 i pretvorite ga u polje tipa float32 i rezultat
   spremite u novo polje. Uočavate li gubitak preciznosti?
 
 * Pretvorite dobiveno polje tipa float32 u polje tipa float64.
   Je li rezultat jednak početnom polju?
 
 * Iskoristite round da na rezultirajućem polju tipa float64 
   dobijete iste vrijednosti kao na početnom.

 Napomena: rezultat ovog zadatka uvelike ovisi o računalu na
 kojem radite.
"""
import numpy as np

a = 9.45 * np.ones((3, 4), dtype=np.float64)
print("a =\n", a)
print(a.dtype)

b = a.astype(np.float32)
print("\nb =\n", b)
print(b.dtype)

c = b.astype(np.float64)
print("\nc =\n", c)
print(c.dtype)

print("\na =?= c\n", a == c)

d = np.round(c, 3)
print("\nd =", d)
print(d.dtype)

print("\na =?= d\n", a == d)
