"""
 * Iskoristite dva dvodimenzionalna polja iz prethodnog zadatka
   da izračunajte 2 * a + b, ali tako da pretvorite drugo u polje
   koje ima elementa tipa numpy.float32.
"""
import numpy as np

a = np.array([[1, 2, 3],
              [4, 5, 6],
              [7, 8, 9]], dtype=np.float32)

b = np.array([[10, 20, 30],
              [40, 50, 60],
              [70, 80, 90]], dtype=np.float64)

print("a:\n", a)
print(a.dtype)

print("\nb:\n", b)
print(b.dtype)

c = b.astype(np.float32)

d = 2 * a + c
print("\n2 * a + b =\n", d)
print(d.dtype)
