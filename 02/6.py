"""
 * Stvorite dvije datoteke, nazovite ih matrica_a.txt i
   matrica_b.txt. Matrica u prvoj datoteci neka bude oblika
   (3, 5), a u drugoj datoteci oblika (5, 4).
 
 * Izvršite čitanje podataka, a zatim izračunajte produkt
   dvaju matrica. Možete li izračunati oba produkta ili
   samo jedan? Objasnite zašto.
"""
import numpy as np

p = open("matrica_a.txt")
m_a = np.loadtxt(p)
p.close() 

print(m_a)

p = open("matrica_b.txt")
m_b = np.loadtxt(p)
p.close() 

print("\n", m_b)

c = np.dot(m_a, m_b)
print("\n", c)

