# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    A = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [11, 22, 33]], dtype=np.float32)
    x = np.array([10, 11, 12], dtype=np.float32)
    y = np.empty(4, dtype=np.float32)
else:
    A = None
    x = np.empty(3, dtype=np.float32)
    y = None

redak_A = np.empty(3, dtype=np.float32)
comm.Scatter(A, redak_A, root=0)
comm.Bcast(x, root=0)

element_y = np.dot(redak_A, x)
comm.Gather(element_y, y, root=0)

if rank == 0:
    print("Produkt matrice\n", A)
    print("i vektora\n", x)
    print("iznosi\n", y)
