"""
 * Modificirajte gornji primjer tako da dodate kod koji mjeri vrijeme
   izvođenja za svaki od procesa.
 
 * Usporedite vrijeme izvođenja algoritma za 2, 3, 4 procesa kad svaki
   proces izvodi 10^4^, 10^5^, 10^6^ iteracija. Opišite svoje zaključke.
"""
# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI
import sys
import time

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

try:
    n_iterations = int(sys.argv[1])
except IndexError:
    print("Niste unijeli broj iteracija, koristi se 10.")
    n_iterations = 10

def compute_pi(n, start=0, step=1):
    h = 1.0 / n
    s = 0.0
    for i in range(start, n, step):
        x = h * (i + 0.5)
        s += 4.0 / (1.0 + x**2)
    return s * h

pi_part = np.empty(1)

start = time.time()

pi_part[0] = compute_pi(n_iterations, start=rank, step=size)

if rank == 0:
    pi = np.empty(1)
else:
    pi = None

comm.Reduce(pi_part, pi, op=MPI.SUM, root=0)

end = time.time()
print("Vrijeme izvođenja za proces", rank, ":", end - start, "s")

if rank == 0:
    error = abs(pi - np.math.pi)
    print("pi is approximately %.16f, error is approximately %.16f" % (pi, error))
