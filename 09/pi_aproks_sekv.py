# varijanta s NumPy poljima
import numpy as np
import sys

try:
    n_iterations = int(sys.argv[1])
except IndexError:
    print("Niste unijeli broj iteracija, koristi se 10.")
    n_iterations = 10

def compute_pi(n):
    h = 1.0 / n
    s = 0.0
    for i in range(n):
        x = h * (i + 0.5)
        s += 4.0 / (1.0 + x**2)
    return s * h

pi = compute_pi(n_iterations)
error = abs(pi - np.math.pi)
print("pi is approximately %.16f, error is approximately %.16f" % (pi, error))
