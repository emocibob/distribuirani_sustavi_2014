"""
 * Proširite navedeni kod da umjesto produkta matrice i vektora računa produkt
   dvaju matrica.
"""
# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    A = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [11, 22, 33]], dtype=np.float32)
    B = np.array([[10, 11, 12, 15], [100, 200, 300, 400], [50, 60, 51, 78]], dtype=np.float32)
    y = np.empty((4, 4), dtype=np.float32)
else:
    A = None
    B = np.empty((3, 4), dtype=np.float32)
    y = None

redak_A = np.empty(3, dtype=np.float32)
comm.Scatter(A, redak_A, root=0)
comm.Bcast(B, root=0)

element_y = np.dot(redak_A, B)
comm.Gather(element_y, y, root=0)

if rank == 0:
    print(A)
    print(B)
    print("Produkt:", y)
    print("Provjera:", y == np.dot(A, B))
