"""
 * Pokrenite kod tako da se izvodi u 8 procesa.
 
 * Učinite da procesi s parnim identifikatorom ispisuju i Ja sam 
   proces sa parnim identifikatorom, a procesi s neparnim
   identifikatorom ispisuju Ja sam proces s neparnim identifikatorom.
   
 * Dodajte da procesi s neparnim identifikatorom pored toga ispisuju
   i Kvadrat mog identifikatora iznosi te kvadrat svog identifikatora.
"""
from mpi4py import MPI

#name = MPI.Get_processor_name()
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank % 2 == 0:
    print("Ja sam proces sa parnim identifikatorom (" + str(rank)  + ").")
else:
    print("Ja sam proces sa neparnim identifikatorom (" + str(rank)  + "). Kvadrat mog identifikatora iznosi " + str(rank**2) + ".")
