"""
 * Napišite program koji se izvodi u tri procesa i koristi
   blokirajuću komunikaciju:

   * proces ranga 1 računa zbroj i produkt parnih prirodnih
     brojeva manjih ili jednakih 10 i rezultat šalje procesu
     ranga 0 kao listu koja sadrži dva elementa tipa int ili
     numpy polje od dva elementa tipa numpy.int32,
 
   * proces ranga 2 računa zbroj i produkt prirodnih brojeva
     manjih ili jednakih 20 i rezultat šalje procesu ranga 0
     kao listu koja sadrži dva elementa tipa int ili kao numpy
     polje od dva elementa tipa numpy.int64,
  
   * proces ranga 0 prima rezultate i ispisuje ih na ekran.
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 3:
    print("Potrebna su barem tri procesa za izvođenje")
    exit()

if rank == 1:
    zbroj = 0
    produkt = 1
    for i in range(2, 11):
        if i % 2 == 0:
            zbroj += i
            produkt *= i
    zbroj = np.int32(zbroj)
    produkt = np.int32(produkt)

    sendmsg = np.array([zbroj, produkt], dtype=np.int32)
    comm.Send(sendmsg, dest=0)

elif rank == 0:
    recvmsg = np.empty(2, dtype=np.int32)
    comm.Recv(recvmsg, source=1)

    print("Ja sam proces #", rank)
    print("od procesa #1 dobio sam:", recvmsg)

    recvmsg = np.empty(2, dtype=np.int64)
    comm.Recv(recvmsg, source=2)

    print("od procesa #2 dobio sam:", recvmsg)

elif rank == 2:
    zbroj = 0
    produkt = 1
    for i in range(1, 21):
        zbroj += i
        produkt *= i
    zbroj = np.int64(zbroj)
    produkt = np.int64(produkt)

    sendmsg = np.array([zbroj, produkt], dtype=np.int64)
    comm.Send(sendmsg, dest=0)

else:
    print("Proces ranga", rank, "ne razmjenjuje poruke")
    exit()
