# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    msg = np.array([1.5, 2.5, 3.5, 4.5])
else:
    msg = np.empty(4)

print("Prije broadcasta proces ranga", rank, "ima vrijednost varijable", msg)
comm.Bcast(msg, root=0)
print("Nakon broadcasta proces ranga", rank, "ima vrijednost varijable", msg)
