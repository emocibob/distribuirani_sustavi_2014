"""
 * Prilagodite kod primjera tako da se broadcastom šalje
   lista s 4 podliste s po 4 elementa, odnosno polje oblika
   (4, 4), u oba slučaja s proizvoljnim vrijednostima.

 * Učinite da zatim proces ranga 2 promijeni vrijednost
   elementa na poziciji (1, 1) tako da je uveća za 5, i
   napravite broadcast s njega svim ostalima.

 * Na svim procesima ispišite primljene podatke na ekran
   da provjerite je li operacija bila uspješna.

 * Dodajte treći broadcast s procesa ranga 1, neka se radi
   o 2D polju s vrijednostima tipa float32 formata (5,15).

 * Svaki od procesa po primitku polja ispisuje na ekran
   vrijednosti na koordinati (rang, rang).
"""
# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    msg = np.array([[1.5, 2.5, 3.5, 4.5],
                    [1.1, 2.1, 3.1, 4.1],
                    [5.0, 6.0, 7.0, 8.0],
                    [0.9, 1.9, 2.9, 3.9]])
else:
    msg = np.zeros((4, 4))

#print("Prije broadcasta proces ranga", rank, "ima vrijednost varijable\n", msg)
comm.Bcast(msg, root=0)
#print("Nakon broadcasta proces ranga", rank, "ima vrijednost varijable\n", msg)

##print("Prije broadcasta proces ranga", rank, "ima vrijednost varijable\n", msg)

if rank == 2:
    msg[1, 1] += 5

comm.Bcast(msg, root=2)

##print("Nakon broadcasta proces ranga", rank, "ima vrijednost varijable\n", msg)


if rank == 1:
    msg2 = np.array([list(range(x, x + 15)) for x in range(5)], dtype=np.float32)
else:
    msg2 = np.zeros((5, 15), dtype=np.float32)

print("Prije broadcasta proces ranga", rank, "ima vrijednost varijable\n", msg2)
comm.Bcast(msg2, root=1)
print("Nakon broadcasta proces ranga", rank, "ima vrijednost varijable\n", msg2)

print("Ja sam proces ranga", rank, ", moji output:", msg2[rank, rank])
