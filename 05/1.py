"""
 * Napišite dvije varijante ovog programa. Prva varijanta
   neka koristi blokirajuću komunikaciju, a druga neblokirajuću.
   Obje varijante izvode se u četiri procesa.

   * Na procesu ranga 0 incijaliziraju se dva vektora veličine
     6 elemenata. Po dva elementa svakog vektora šalju se
     procesima ranga 1, 2 i 3.
    
   * Procesi ranga 1, 2 i 3 vrše zbroj odgovarajućih elemenata
     vektora i rezultat šalju procesu ranga 0.
   
   * Proces ranga 0 prima rezultate od procesa ranga 1, 2 i 3
     i vrši provjeru točnosti rješenja.
"""
# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 4:
    print("Potrebno je barem 4 procesa.")
    exit()

if rank == 0:
    a = np.array([1.0, 2.0, 3.0, 9.0, 10.0, 15.0])
    b = np.array([4.0, 5.0, 6.0, 7.0, 11.0, 19.0])
    zbroj = np.empty(6)

    comm.Send(a[0:2], dest=1, tag=0)
    comm.Send(b[0:2], dest=1, tag=1)

    comm.Send(a[2:4], dest=2, tag=0)
    comm.Send(b[2:4], dest=2, tag=1)

    comm.Send(a[4:6], dest=3, tag=0)
    comm.Send(b[4:6], dest=3, tag=1)

    comm.Recv(zbroj[0:2], source=1)
    comm.Recv(zbroj[2:4], source=2)
    comm.Recv(zbroj[4:6], source=3)

    print("Zbroj je", zbroj)

    zbroj_provjera = np.empty(6)

    for i in range(6):
        zbroj_provjera[i] = a[i] + b[i]

    # zbroj_provjera = a + b

    print("Provjera prolazi:", zbroj == zbroj_provjera)

elif rank == 1:
    dio_a = np.empty(2)
    dio_b = np.empty(2)
    comm.Recv(dio_a, source=0, tag=0)
    comm.Recv(dio_b, source=0, tag=1)

    zbroj = dio_a + dio_b
    comm.Send(zbroj, dest=0)

elif rank == 2:
    dio_a = np.empty(2)
    dio_b = np.empty(2)
    comm.Recv(dio_a, source=0, tag=0)
    comm.Recv(dio_b, source=0, tag=1)

    zbroj = dio_a + dio_b
    comm.Send(zbroj, dest=0)

elif rank == 3:
    dio_a = np.empty(2)
    dio_b = np.empty(2)
    comm.Recv(dio_a, source=0, tag=0)
    comm.Recv(dio_b, source=0, tag=1)

    zbroj = dio_a + dio_b
    comm.Send(zbroj, dest=0)

else:
    exit()
