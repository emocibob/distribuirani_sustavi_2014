import numpy as np
# import random
import time
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

sleep_time = 10 * np.random.random()
# sleep_time = 10 * random.random()
print("Proces ranga", rank, "spavat će", sleep_time, "sekundi")
time.sleep(sleep_time)

comm.Barrier()

print("Proces ranga", rank, "prošao je barijeru")
